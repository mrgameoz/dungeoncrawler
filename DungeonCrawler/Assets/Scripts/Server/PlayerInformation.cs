﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInformation : MonoBehaviour
{
    [SerializeField]
    public List<CharacterController> PlayersConnected = new List<CharacterController>();

    private void Awake()
    {
        CharacterController.playerCreated += PlayerCreated;
        CharacterController.playerDestroyed += PlayerDestroyed;
    }

    private void PlayerCreated(CharacterController player)
    {
        PlayersConnected.Add(player);
    }

    private void PlayerDestroyed(CharacterController player)
    {
        PlayersConnected.Remove(player);
    }

    private void OnDestroy()
    {
        CharacterController.playerCreated -= PlayerCreated;
        CharacterController.playerDestroyed -= PlayerDestroyed;
    }
}
