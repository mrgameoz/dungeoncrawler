﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public abstract class UnitStatSystem : NetworkBehaviour
{
    private UnitMovementSystem _unitMovementSystem;
    protected UnitAnimationSystem UnitAnimationSystem;

    [SyncVar]
    public UnitState UnitState;

    [Header("Base Stats")]
    [SerializeField]
    [SyncVar]
    protected BaseStats BaseStats;
    public BaseStats BaseStatsValue { get { return BaseStats; } }

    #region Current Stats
    [Header("Currrent Stats")]
    [SerializeField]
    [SyncVar(hook = nameof(HealthChanged))]
    public int CurrentHealth;
    [SerializeField]
    [SyncVar(hook = nameof(HealthChanged))]
    public int CurrentMana;
    [SyncVar]
    protected int CurrentShield;
    [SyncVar]
    protected int CurrentDamage;
    [SerializeField]
    [SyncVar]
    protected float CurrentAttackSpeed;
    [SerializeField]
    [SyncVar]
    protected float CurrentMovementSpeed;
    [SyncVar]
    protected float CurrentCooldownReduction;
    [SyncVar]
    protected float CurrentCritChance;
    [SyncVar]
    protected float CurrentDamageReduction;

    protected readonly SyncList<StatModifier> StatModifierList = new SyncList<StatModifier>();
    private List<StatModifier> _dirtyStatModifierList = new List<StatModifier>();

    #endregion

    //Health Regeneration
    protected Timer HealthRegenTimer;
    protected float SecondsToRegenHealth = 5f;//if unit doesnt take damage for 10seconds switch to out of combat

    //In Combat Status 
    protected Timer CombatTimer;
    private float _outOfCombatTime = 10f;//if unit doesnt take damage for 10seconds switch to out of combat
    //[SerializeField]
    //protected UnitStatusBar unitWorldSpaceUI;
    private void Awake()
    {
        _unitMovementSystem = GetComponent<UnitMovementSystem>();
        UnitAnimationSystem = GetComponent<UnitAnimationSystem>();
        HealthRegenTimer = new Timer(RegenerateHealth);
        CombatTimer = new Timer(OutOfCombat);
    }

    public override void OnStartServer()
    {
        InitStats();
    }

    protected virtual void InitStats()
    {
        UnitState = UnitState.Alive;
    }

    public virtual void Revive()
    {
        UnitState = UnitState.Alive;
        CurrentHealth = BaseStats.Health;
        CurrentMana = BaseStats.Mana;
    }
    // Safe Update is called on controller Update() this update only runs on server
    public virtual void SafeUpdate()
    {
        UpdateStatModifiers();
        CombatTimer.Update();
        HealthRegenTimer.Update();
    }

    #region Health Changes

    protected virtual void HealthChanged(int oldHealth,int newHealth)
    {
        //unitWorldSpaceUI.UpdateHealthBar((float)newHealth / (float)BaseStatsValue.Health);
    }

    [ContextMenu("Do Damage")]
    public void DoDamage()
    {
        CmdDoDamage();
    }

    [Command]
    private void CmdDoDamage()
    {
        ServerTakeDamage(100);
    }

    [ServerCallback]
    public virtual void ServerTakeDamage(int damage)
    {
        if(UnitState==UnitState.Invulnerable || UnitState == UnitState.Dead)
        {
            return;
        }
        UnitState = UnitState.InCombat;
        StopHealthRegen();
        CurrentHealth -= damage;
        CombatTimer.StartTimer(_outOfCombatTime);
        if (CurrentHealth <= 0)
        {
            CurrentHealth = 0;
            Dead();
        }
    }

    [Command]
    public virtual void CmdTakeDamage(int damage)
    {
        ServerTakeDamage(damage);
    }


    [ContextMenu("Do Heal")]
    private void DoHeal()
    {
        CmdHealPlayer(1000);
    }

    [ServerCallback]
    public virtual void ServerHealPlayer(int heal)
    {
        if (UnitState == UnitState.InCombat || UnitState == UnitState.Dead)
        {
            return;
        }
        CurrentHealth += heal;
        if (CurrentHealth > BaseStats.Health)
        {
            CurrentHealth = BaseStats.Health;
            StopHealthRegen();
        }
    }

    [Command]
    public virtual void CmdHealPlayer(int heal)
    {
        ServerTakeDamage(heal);
    }
    #endregion

    #region StatModifiers

    [ServerCallback]
    private void UpdateStatModifiers()
    {
        if (StatModifierList.Count == 0)
        {
            return;
        }
        ClearDirtyModifiers();
        for (int i = 0; i < StatModifierList.Count; i++)
        {
            StatModifierList[i].Update(Time.deltaTime);
            if(StatModifierList[i].IsFinished())
            {
                RemoveStatModifier(StatModifierList[i]);
                _dirtyStatModifierList.Add(StatModifierList[i]);
            }
        }
    }

    private void ClearDirtyModifiers()
    {
        if (_dirtyStatModifierList.Count == 0)
        {
            return;
        }
        for (int i = 0; i < _dirtyStatModifierList.Count; i++)
        {
            if (StatModifierList.Contains(_dirtyStatModifierList[i]))
            {
                StatModifierList.Remove(_dirtyStatModifierList[i]);
            }
        }
        _dirtyStatModifierList.Clear();
    }

    public void AddStatModifier(StatModifier statModifier)
    {
        CurrentHealth += statModifier.Stats.Health;
        CurrentMana += statModifier.Stats.Mana;
        CurrentShield += statModifier.Stats.Shield;
        CurrentDamage += statModifier.Stats.Damage;
        CurrentMovementSpeed += statModifier.Stats.MovementSpeed;
        CurrentAttackSpeed += statModifier.Stats.AttackSpeed;
        CurrentCooldownReduction += statModifier.Stats.CooldownReduction;
        CurrentCritChance += statModifier.Stats.CritChance;
        CurrentDamageReduction += statModifier.Stats.DamageReduction;
        statModifier.ApplyModifier();
        StatModifierList.Add(statModifier);
    }

    protected void RemoveStatModifier(StatModifier statModifier)
    {
        CurrentHealth -= statModifier.Stats.Health;
        CurrentMana -= statModifier.Stats.Mana;
        CurrentShield -= statModifier.Stats.Shield;
        CurrentDamage -= statModifier.Stats.Damage;
        CurrentMovementSpeed -= statModifier.Stats.MovementSpeed;
        CurrentAttackSpeed -= statModifier.Stats.AttackSpeed;
        CurrentCooldownReduction -= statModifier.Stats.CooldownReduction;
        CurrentCritChance -= statModifier.Stats.CritChance;
        CurrentDamageReduction -= statModifier.Stats.DamageReduction;
    }


    #endregion

    #region States

    private void Dead()
    {
        Debug.Log("DEad");
        UnitState = UnitState.Dead;
        UnitAnimationSystem.PlayDeadAnimation(true);
        _unitMovementSystem.StopMovement();
    }


    #endregion

    #region UnitRegenaration

    [ServerCallback]
    protected virtual void CheckIfHealthRegenIsRequired()
    {
        if (!HealthRegenTimer.Active && UnitState == UnitState.Alive && CurrentHealth < BaseStats.Health)
        {
            HealthRegenTimer.StartTimer(SecondsToRegenHealth,true);
        }
    }

    [ServerCallback]
    protected void StopHealthRegen()
    {
        if(HealthRegenTimer.Active)
        {
            HealthRegenTimer.StopTimer();
        }
    }



    [ServerCallback]
    private void RegenerateHealth()
    {
        if(UnitState == UnitState.Alive)
        {
            ServerHealPlayer(20);
        }
    }

    #endregion


    #region CombatTimer

    private void OutOfCombat()
    {
        if (UnitState == UnitState.InCombat)
        {
            UnitState = UnitState.Alive;
            CheckIfHealthRegenIsRequired();
        }
    }

    #endregion
}

public enum UnitState { None, Alive, InCombat, Dead, Invulnerable, Immovable, Stunned };

