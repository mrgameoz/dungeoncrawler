﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class UnitLookAtRotation :NetworkBehaviour
{
    [Command]
    public void CmdLookAtDirection(Vector3 point)
    {
        LookAtDirection(point);
    }

    [ServerCallback]
    public void LookAtDirection(Vector3 point)
    {
        Vector3 mousePos = point;
        Vector3 objectPos = transform.position;
        Vector3 direction = Vector3.zero;
        direction.x = mousePos.x - objectPos.x;
        direction.z = mousePos.z - objectPos.z;
        direction = direction.normalized;
        float angle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;

        transform.rotation = Quaternion.Euler(new Vector3(0, angle, 0));
    }



}
