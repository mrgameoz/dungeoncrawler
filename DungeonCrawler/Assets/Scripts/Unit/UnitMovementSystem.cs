﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.AI;

public class UnitMovementSystem : NetworkBehaviour
{

    private NavMeshAgent _navMeshAgent;
    private Transform _target;
    [SerializeField]
    private float _targetChaseRange = 5;
    [SerializeField]
    private float _moveSpeed;

    private UnitAnimationSystem _unitAnimationSystem;
    private NetworkTransform _networkTransform;

    private void Awake()
    {
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _unitAnimationSystem = GetComponent<UnitAnimationSystem>();
        _networkTransform = GetComponent<NetworkTransform>();
    }

    // Safe Update is called on controller Update() this update only runs on server
    public virtual void SafeUpdate()
    {
        AdjustMovement();
    }

    public void DisableNavmeshAgent(bool disable)
    {
        _navMeshAgent.enabled = !disable;
    }

    public virtual void SetMovementSpeed(float speed)
    {
        _moveSpeed = speed;
        _navMeshAgent.speed = _moveSpeed;
        _navMeshAgent.acceleration = _moveSpeed * 5;//accelration is 5x speed for default
    }

    #region Server
    [ServerCallback]
    private void AdjustMovement()
    {
        if (_target)
        {
            if ((_target.position - transform.position).sqrMagnitude > _targetChaseRange * _targetChaseRange)
            {
                StartMovement(_target.position);
            }

            if ((_target.position - transform.position).sqrMagnitude <= _targetChaseRange * _targetChaseRange)
            {
                StopMovement();
            }
        }
        if (!_navMeshAgent.pathPending)
        {
            if (_navMeshAgent.remainingDistance <= _navMeshAgent.stoppingDistance)
            {
                if (!_navMeshAgent.hasPath || _navMeshAgent.velocity.sqrMagnitude == 0f)
                {
                    StopMovement();
                }
            }
        }
    }

    [ServerCallback]
    public void SetStoppingDistance(float distance)
    {
        _navMeshAgent.stoppingDistance = distance;
    }

    [ServerCallback]
    public void RepositionPlayer(Vector3 pos)
    {
        StopMovement();
        _networkTransform.ServerTeleport(pos);
        _navMeshAgent.Warp(pos);
        ResetPathingWhilePositionChange(pos);
    }

    private void ResetPathingWhilePositionChange(Vector3 pos)
    {
        StopMovement();
        _navMeshAgent.nextPosition = pos;
        _navMeshAgent.SetDestination(pos);
    }

    [ServerCallback]
    public void StopMovement()
    {
        _navMeshAgent.ResetPath();
        _navMeshAgent.velocity = Vector3.zero;
        _unitAnimationSystem.UnitMoveAnimationSpeed(0);
    }

    [ServerCallback]
    public void StartMovement(Vector3 position)
    {
        _navMeshAgent.SetDestination(position);
        _unitAnimationSystem.UnitMoveAnimationSpeed(1);
    }

    [Command]
    public void CmdMoveToPoint(Vector3 position)
    {
        _target = null;
        if (!NavMesh.SamplePosition(position, out NavMeshHit hit, 1, NavMesh.AllAreas))
        {
            return;
        }
        StartMovement(position);
    }

    [Command]
    public void CmdSetTarget(GameObject target)
    {
        SetTarget(target);
    }

    [ServerCallback]
    public void SetTarget(GameObject target)
    {
        _target = target.transform;
        StartMovement(_target.position);
    }

    [ServerCallback]
    public void RemoveTarget()
    {
        _target = null;
        StopMovement();
    }

    [Command]
    public void CmdStopMovement()
    {
        StopMovement();
    }

    public bool PathFinished()
    {
        return _navMeshAgent.pathStatus == NavMeshPathStatus.PathComplete;
    }
    #endregion
}
