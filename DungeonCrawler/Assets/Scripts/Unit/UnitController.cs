﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class UnitController : NetworkBehaviour
{

    public UnitStatSystem UnitStatSystem;
    public UnitMovementSystem UnitMovementSystem;
    public UnitLookAtRotation UnitLookAtRotation;
    public UnitAbilitySystem UnitAbilitySystem;
    public UnitAnimationSystem UnitAnimationSystem;

    protected virtual void Awake()
    {
        UnitStatSystem = GetComponent<UnitStatSystem>();
        UnitMovementSystem = GetComponent<UnitMovementSystem>();
        UnitMovementSystem.SetMovementSpeed(UnitStatSystem.BaseStatsValue.MovementSpeed);
        UnitLookAtRotation = GetComponent<UnitLookAtRotation>();
        UnitAnimationSystem = GetComponent<UnitAnimationSystem>();
        if (TryGetComponent<UnitAbilitySystem>(out UnitAbilitySystem unitAbility))
        {
            UnitAbilitySystem = unitAbility;
        }

    }

    // Start is called before the first frame update
    protected virtual void Start()
    {
        
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if(UnitStatSystem.UnitState!=UnitState.Dead)
        {
            UnitStatSystem.SafeUpdate();
            if (UnitAbilitySystem != null)
            {
                UnitAbilitySystem.SafeUpdate();
            }
            UnitMovementSystem.SafeUpdate();
        }
        UnitAnimationSystem.SafeUpdate();
    }


}
