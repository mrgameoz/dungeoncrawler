﻿
public interface IUnitTargetHandler 
{
    void DiscoverTargetToLook();
    void DiscoverTargetToMove();

}
