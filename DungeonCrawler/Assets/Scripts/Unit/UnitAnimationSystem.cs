﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class UnitAnimationSystem : NetworkBehaviour
{

    [SerializeField]
    protected Animator Animator;
    [SerializeField]
    protected NetworkAnimator NetworkAnimator;

    //attacking
    protected int BasicAttackAnim = Animator.StringToHash("BasicAttack");

    //walking
    private int _animMoveSpeedID = Animator.StringToHash("MovementSpeed");
    private float _animMoveSpeed;

    //dead
    private int _animDeadID = Animator.StringToHash("Dead");

    // Safe Update is called on controller Update() this update only runs on server
    public virtual void SafeUpdate()
    {
        PlayerMoveAnimation();
    }

    [ServerCallback]
    private void PlayerMoveAnimation()
    {
        Animator.SetFloat(_animMoveSpeedID, _animMoveSpeed, 0.2f, Time.deltaTime * 2f);
    }

    public virtual void UnitMoveAnimationSpeed(float speed)
    {   
        _animMoveSpeed = speed;
    }

    [ServerCallback]
    public virtual void TriggerBasicAttackAnimation()
    {
        NetworkAnimator.SetTrigger(BasicAttackAnim);
    }

    [ServerCallback]
    public void PlayDeadAnimation(bool play)
    {
        Animator.SetBool(_animDeadID, play);
    }
}
