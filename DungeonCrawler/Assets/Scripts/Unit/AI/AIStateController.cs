﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class AIStateController : NetworkBehaviour
{
    [SerializeField]
    private EnemyData _enemyData;
    [SerializeField]
    private GameObject AITaskComponentObject;
    private Dictionary<AIStates, ITask> _tasks = new Dictionary<AIStates, ITask>();
    [SerializeField]
    private ITask _currentTask;
    [SerializeField]
    private AIStates _currentState;

    private void Awake()
    {
        ObserveTask observe;
        if (AITaskComponentObject.TryGetComponent<ObserveTask>(out observe))
        {
            observe.SetTaskRange(_enemyData.ObserveRange);
            _tasks.Add(AIStates.Observe, observe);
            observe.NextState = AIStates.Chase;
        }

        ChaseTask chase;
        if (AITaskComponentObject.TryGetComponent<ChaseTask>(out chase))
        {
            chase.SetTaskRange(_enemyData.AttackRange);
            chase.SetFleeRange(_enemyData.FleeRange);
            _tasks.Add(AIStates.Chase, chase);
            chase.NextState = AIStates.Flee;
        }

        AttackTask attack;
        if (AITaskComponentObject.TryGetComponent<AttackTask>(out attack))
        {
            attack.SetTaskRange(_enemyData.AttackRange);
            _tasks.Add(AIStates.Attack, attack);
            attack.NextState = AIStates.Observe;
        }

        FleeTask flee;
        if (AITaskComponentObject.TryGetComponent<FleeTask>(out flee))
        {
            flee.SetTaskRange(_enemyData.FleeRange);
            _tasks.Add(AIStates.Flee, flee);
            flee.NextState = AIStates.Observe;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        SwitchTask(AIStates.Observe);
    }

    // Update is called once per frame
    public void SafeUpdate()
    {
        TaskUpdate();
    }

    [ServerCallback]
    private void TaskUpdate()
    {
        if (_currentTask == null)
        {
            return;
        }
        _currentTask.SafeUpdate();
        if (_currentTask.TaskCompleted())
        {
            Debug.Log("Task Completed "+ _currentState.ToString());
            SwitchTask(_currentTask.NextState);
        }
    }

    private void SwitchTask(AIStates state)
    {
        _currentTask?.Exit();
        _currentState = state;
        _currentTask = _tasks[_currentState];
        _currentTask?.Enter();
    }
}

public enum AIStates { Observe, Chase, Attack, Flee, Patrol };
