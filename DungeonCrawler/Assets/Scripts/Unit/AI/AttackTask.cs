﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackTask : MonoBehaviour, ITask
{
    public Transform mTransform { get; set; }
    public Transform TargetPlayer { get; set; }
    public AIStates NextState { get; set; }
    private bool _attackAnimationCompleted;
    [SerializeField]
    private EnemyAbilitySystem _enemyAbilitySystem;
    private float _range;

    private void OnEnable()
    {
        _enemyAbilitySystem.Event_AbilityAnimationCompleted += AttackAnimationCooldownCompleted;
    }

    private void OnDisable()
    {
        _enemyAbilitySystem.Event_AbilityAnimationCompleted -= AttackAnimationCooldownCompleted;
    }

    public void Enter()
    {
        _attackAnimationCompleted = false;
        _enemyAbilitySystem.InitateAttack();
    }

    public void Exit()
    {

    }

    public void SafeUpdate()
    {
    }

    public bool TaskCompleted()
    {
        return _attackAnimationCompleted;
    }

    private void AttackAnimationCooldownCompleted()
    {
        _attackAnimationCompleted = true;
    }

    public void SetTaskRange(float range)
    {
        _range = range;
    }
}
