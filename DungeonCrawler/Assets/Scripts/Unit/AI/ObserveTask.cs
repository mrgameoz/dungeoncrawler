﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class ObserveTask : MonoBehaviour, ITask
{
    public Transform mTransform { get; set; }
    private List<Transform> _playerList = new List<Transform>();
    public Transform TargetPlayer { get; set; }
    public AIStates NextState { get; set; }
    private ChaseTask _chaseTask;

    [Space]
    [SerializeField]
    private UnitLookAtRotation _unitLookAtRotation;
    [SerializeField]
    private float _proximity = 1f;
    private float _range;
    [SerializeField]
    private LayerMask _layerMask;
    [SerializeField]
    private Targetable _targetable;

    public Action<GameObject> Event_TargetFound;

    private void Awake()
    {
        mTransform = transform;
        _chaseTask = GetComponent<ChaseTask>();
    }

    public void Enter()
    {
         StartCoroutine(ObserveAreaAroundYou());
    }

    public void Exit()
    {
        StopCoroutine(ObserveAreaAroundYou());
        Event_TargetFound?.Invoke(TargetPlayer.gameObject);
        TargetPlayer = null;
    }

    private IEnumerator ObserveAreaAroundYou()
    {
        for ( ; ; )
        {
            yield return new WaitForSeconds(_proximity);
            CheckForPlayerObject();
        }
    }

    protected virtual void CheckForPlayerObject()
    {
        if (TargetPlayer == null)
        {
            Collider[] objects = Physics.OverlapSphere(mTransform.position, _range, _layerMask);
            //check for objects in radius
            for (int i = 0; i < objects.Length; i++)
            {
                if (objects[i].transform.TransformIsPlayer())
                {
                    //check if the found object is in line of sight
                    Ray ray = new Ray();
                    RaycastHit visionHit;
                    ray.origin = _targetable.GetUnitTargetPosition();
                    ray.direction = objects[i].GetComponent<Targetable>().GetUnitTargetPosition() - _targetable.GetUnitTargetPosition();
                    if (Physics.Raycast(ray,out visionHit, Mathf.Infinity,_layerMask))
                    {
                        if(visionHit.transform == objects[i].transform)
                        {
                            TargetPlayer = objects[i].transform;
                            _unitLookAtRotation.LookAtDirection(TargetPlayer.position);
                            break;
                        }

                    }

                }
            }
        }
    }

    public void SafeUpdate()
    {
        SetTarget();
    }

    public bool TaskCompleted()
    {
        return TargetPlayer != null;
    }

    public void SetTarget()
    {
        if (TargetPlayer == null & _playerList.Count > 0)
        {
            TargetPlayer = _playerList[0];
        }
    }

    public void SetTaskRange(float range)
    {
        _range = range;
    }
}
