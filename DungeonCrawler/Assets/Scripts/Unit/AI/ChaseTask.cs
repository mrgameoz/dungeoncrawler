﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseTask : MonoBehaviour, ITask
{
    public Transform mTransform { get; set; }
    public Transform TargetPlayer { get; set; }
    public AIStates NextState { get; set; }
    [SerializeField]
    private UnitMovementSystem _unitMovementSystem;
    [SerializeField]
    private EnemyAbilitySystem _enemyAbilitySystem;
    private Vector3 _spawnPos;
    private float _attackRange;
    private float _fleeRange;

    private void Awake()
    {
        mTransform = transform;
        _spawnPos = mTransform.position;
    }

    public void Enter()
    {
        _unitMovementSystem.SetStoppingDistance(_attackRange);

        if(!VectorUtil.IsInRange(mTransform.position,TargetPlayer.position,_attackRange))//if not in range move to target
        {
            _unitMovementSystem.SetTarget(TargetPlayer.gameObject);
        }
    }

    public void Exit()
    {
        _unitMovementSystem.RemoveTarget();
    }

    public void SafeUpdate()
    {
    }

    public bool TaskCompleted()
    {
        bool taskCompleted = false;
        float distanceFromPlayer = Vector3.Distance(TargetPlayer.position, mTransform.position);
        if (!VectorUtil.IsInRange(_spawnPos, mTransform.position,_fleeRange))//if enemy is far away from spawn point return back to spawn point
        {
            NextState = AIStates.Flee;
            taskCompleted = true;
        }
        //attack player if player is in range
        else if(VectorUtil.IsInRange(distanceFromPlayer, _attackRange) && _enemyAbilitySystem.GlobalAbilityCooldown <= 0)
        {
            NextState = AIStates.Attack;
            taskCompleted = true;
        }
        return taskCompleted;
    }

    public void SetTarget(GameObject target)
    {
        TargetPlayer = target.transform;
    }

    public void SetTaskRange(float range)
    {
        _attackRange = range;
    }

    public void SetFleeRange(float range)
    {
        _fleeRange = range;
    }
}
