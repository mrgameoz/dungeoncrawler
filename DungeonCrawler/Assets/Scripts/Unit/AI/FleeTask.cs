﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FleeTask : MonoBehaviour,ITask
{
    public Transform mTransform { get; set; }
    private Vector3 _spawnPos;
    public Transform TargetPlayer { get; set; }
    public AIStates NextState { get; set; }
    [SerializeField]
    private UnitMovementSystem _unitMovementSystem;
    private float _range;

    private void Awake()
    {
        mTransform = transform;
        _spawnPos = mTransform.position;
    }

    public void Enter()
    {
        _unitMovementSystem.SetStoppingDistance(0);
        StopCoroutine(ReturnToSpawnPoint());
        StartCoroutine(ReturnToSpawnPoint());
    }

    private IEnumerator ReturnToSpawnPoint()
    {
        yield return new WaitForSeconds(0.3f);
        _unitMovementSystem.StartMovement(_spawnPos);
    }

    public void Exit()
    {
    }

    public void SafeUpdate()
    {

    }

    public bool TaskCompleted()
    {
        return _unitMovementSystem.PathFinished();
        //return VectorUtil.IsInRange(_spawnPos,mTransform.position,0.01f);
    }

    public void SetTaskRange(float range)
    {
        _range = range;
    }
}
