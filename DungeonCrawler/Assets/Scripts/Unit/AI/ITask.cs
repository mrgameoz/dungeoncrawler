﻿using System;
using UnityEngine;

public interface ITask
{
    Transform mTransform { get; set; }
    Transform TargetPlayer { get; set; }
    AIStates NextState { get; set; }
    void Enter();
    void SafeUpdate();
    void SetTaskRange(float range);
    void Exit();
    bool TaskCompleted();
}
