﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class UnitAbilityPool 
{
    private GameObject _prefabRef;
    private int _maxCount;
    public readonly Queue<GameObject> _pool = new Queue<GameObject>();

    public UnitAbilityPool(GameObject prefab, int maxCount)
    {
        _prefabRef = prefab;
        _maxCount = maxCount;
        ClientScene.RegisterPrefab(_prefabRef, SpawnHandler, UnspawnHandler);
    }

    GameObject SpawnHandler(SpawnMessage msg)
    {
        return GetFromPool(msg.position, msg.rotation);
    }

    // used by ClientScene.RegisterPrefab
    void UnspawnHandler(GameObject spawned)
    {
        PutBackInPool(spawned);
    }

    /// <summary>
    /// Used to take Object from Pool.
    /// <para>Should be used on server to get the next Object</para>
    /// <para>Used on client by ClientScene to spawn objects</para>
    /// </summary>
    /// <param name="position"></param>
    /// <param name="rotation"></param>
    /// <returns></returns>
    public GameObject GetFromPool(Vector3 position, Quaternion rotation)
    {
        GameObject next = _pool.Dequeue();

        // CreateNew might return null if max size is reached
        if (next == null) { return null; }

        // set position/rotation and set active
        next.transform.position = position;
        next.transform.rotation = rotation;
        next.SetActive(true);
        return next;
    }

    /// <summary>
    /// Used to put object back into pool so they can b
    /// <para>Should be used on server after unspawning an object</para>
    /// <para>Used on client by ClientScene to unspawn objects</para>
    /// </summary>
    /// <param name="spawned"></param>
    public void PutBackInPool(GameObject spawned)
    {
        // disable object
        spawned.SetActive(false);

        // add back to pool
        _pool.Enqueue(spawned);
    }
}
