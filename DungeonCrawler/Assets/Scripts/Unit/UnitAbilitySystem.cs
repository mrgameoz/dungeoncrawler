﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.InputSystem;
public class UnitAbilitySystem : NetworkBehaviour
{
    protected UnitStatSystem UnitStatSystem;
    protected UnitAnimationSystem UnitAnimationSystem;

    [SerializeField]
    protected List<BaseSkills> AbilityReference = new List<BaseSkills>();
    [SerializeField]
    public readonly SyncList<BaseSkills> AbilityList = new SyncList<BaseSkills>();
    [SyncVar]
    public GameObject TargetGameObject;
    [SyncVar]
    [HideInInspector]
    public Vector3 TargetPoint;
    public bool TargetInLineOfSight;
    protected Transform mTransform;
    public UnitMovementSystem UnitMovementSystem;
    public UnitLookAtRotation UnitLookAtRotation;
    public NetworkTransform NetworkTransform;

    [Header("Abilities")]
    protected List<Action> AbilityFunctions = new List<Action>();

    [SyncVar]
    private bool isCasting;
    public bool IsCasting { get { return isCasting; } }

    protected float AnimationCooldown;
    public Action Event_AbilityAnimationCompleted;

    //Abilities CDs
    [SyncVar]
    public float AbilityOne_CD;
    [SyncVar]
    public float AbilityTwo_CD;
    [SyncVar]
    public float AbilityThree_CD;
    [SyncVar]
    public float AbilityFour_CD;
    [SyncVar]
    public float AbilityFive_CD;
    [SyncVar]
    public float AbilitySix_CD;

    protected virtual void Awake()
    {
        mTransform = transform;
        UnitStatSystem = GetComponent<UnitStatSystem>();
        UnitAnimationSystem = GetComponent<UnitAnimationSystem>();
        UnitMovementSystem = GetComponent<UnitMovementSystem>();
        UnitLookAtRotation = GetComponent<UnitLookAtRotation>();
        NetworkTransform = GetComponent<NetworkTransform>();

        AbilityFunctions.Add(CastAbilityOne);
        AbilityFunctions.Add(CastAbilityTwo);
        AbilityFunctions.Add(CastAbilityThree);
        AbilityFunctions.Add(CastAbilityFour);
        AbilityFunctions.Add(CastAbilityFive);
        AbilityFunctions.Add(CastAbilitySix);
    }

    // Start is called before the first frame update
    protected virtual void Start()
    {
        CreateAbilities();
        ServerStart();
    }

    //Start is called before the first frame update on Server
    protected virtual void ServerStart()
    {

    }

    // Safe Update is called on controller Update() this update only runs on server
    public virtual void SafeUpdate()
    {
        UpdateAnimationCooldown();
        UpdateAbilitiesState();
    }

    [ServerCallback]
    private void UpdateAnimationCooldown()
    {
        if (AnimationCooldown > 0)
        {
            AnimationCooldown -= Time.deltaTime;
            if (AnimationCooldown <= 0)
            {
                Event_AbilityAnimationCompleted?.Invoke();
                AnimationCooldown = 0;
            }
        }
    }

    #region Server
    [ServerCallback]
    private void CreateAbilities()
    {
        for (int i = 0; i < AbilityReference.Count; i++)
        {
            var ability = Instantiate(AbilityReference[i]);
            ability.name = AbilityReference[i].name;
            AbilityList.Add(ability);
        }
    }

    [ServerCallback]
    protected virtual void UpdateAbilitiesState()
    {
        for (int i = 0; i < AbilityList.Count; i++)
        {
            AbilityList[i].AbilityUpdate(Time.deltaTime);
        }    
    }

    protected virtual void CastAbilityOne()
    {

    }
    protected virtual void CastAbilityTwo()
    {

    }

    protected virtual void CastAbilityThree()
    {

    }

    protected virtual void CastAbilityFour()
    {
        
    }

    protected virtual void CastAbilityFive()
    {

    }

    protected virtual void CastAbilitySix()
    {

    }

    [Command]
    public virtual void CmdSetTarget(GameObject target)
    {
        SetTarget(target);
    }

    [ServerCallback]
    public virtual void SetTarget(GameObject target)
    {
        TargetGameObject = target;
    }

    [Command]
    public virtual void CmdSetTargetPoint(Vector3 target)
    {
        TargetPoint = target;
    }

    [Command]
    public virtual void CmdSetLineOfSightInfo(bool targetInLineOfSight)
    {
        TargetInLineOfSight = targetInLineOfSight;
    }
    #endregion

}
