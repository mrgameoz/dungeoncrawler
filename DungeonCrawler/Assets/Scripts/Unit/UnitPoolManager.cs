﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class UnitPoolManager : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> _abilityPrefabReferences = new List<GameObject>();
    [SerializeField]
    private List<int> _abilityPoolCount = new List<int>();
    //lenght of _abilityPrefabReferences and _abilityPoolCount should be same in most cases so we can compare with ability index

    private Dictionary<int,UnitAbilityPool> _pool = new Dictionary<int, UnitAbilityPool>();

    private void Start()
    {
        InitPoolObjects();
        CreatePool();
    }

    private void InitPoolObjects()
    {
        for (int i = 0; i < _abilityPrefabReferences.Count; i++)
        {
            UnitAbilityPool poolObject = new UnitAbilityPool(_abilityPrefabReferences[i], _abilityPoolCount[i]); ;
            _pool.Add(i, poolObject);
        }
    }

    private void CreatePool()
    {
        for (int i = 0; i < _abilityPoolCount.Count; i++)
        {
            for (int j = 0; j < _abilityPoolCount[i]; j++)
            {
                CreateObject(i);
            }
        }
    }

    private GameObject CreateObject(int abilityID)
    {
        GameObject go = Instantiate(_abilityPrefabReferences[abilityID]);
        go.SetActive(false);
        _pool[abilityID]._pool.Enqueue(go);
        return go;
    }

    public GameObject GetGameObjectFromPool(int abilityID,Vector3 position,Quaternion rotation)
    {
        GameObject go = null;
        if(abilityID < _pool.Count)
        {
            go = _pool[abilityID].GetFromPool(position, rotation);
        }
        return go;
    }

    public void AddToPool(int abilityID, GameObject go)
    {
        if (abilityID < _pool.Count)
        {
            _pool[abilityID].PutBackInPool(go);
        }
    }
}
