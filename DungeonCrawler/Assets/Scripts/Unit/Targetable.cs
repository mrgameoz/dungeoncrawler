﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class Targetable : NetworkBehaviour
{
    [SerializeField]
    private Transform _target;
    
    public Transform GetUnitTargetTransform()
    {
        return _target;
    }

    public Vector3 GetUnitTargetPosition()
    {
        return _target.position;
    }

}
