﻿using System;
using UnityEngine;
using Mirror;
using Mirror.Discovery;
using System.Net;

public class WNetworkDiscovery : NetworkDiscovery
{
    public Action<ServerResponse> OnWServerFound;

    protected override void ProcessResponse(ServerResponse response, IPEndPoint endpoint)
    {
        //base.ProcessResponse(response, endpoint);
        // we received a message from the remote endpoint
        response.EndPoint = endpoint;

        // although we got a supposedly valid url, we may not be able to resolve
        // the provided host
        // However we know the real ip address of the server because we just
        // received a packet from it,  so use that as host.
        UriBuilder realUri = new UriBuilder(response.uri)
        {
            Host = response.EndPoint.Address.ToString()
        };
        response.uri = realUri.Uri;
        OnServerFound.Invoke(response);
        OnWServerFound?.Invoke(response);
    }

    [ContextMenu("Advertise Server")]
    public void AdvertiseAgain()
    {
        AdvertiseServer();
    }
}
