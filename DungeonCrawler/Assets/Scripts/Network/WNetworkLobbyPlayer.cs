﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class WNetworkLobbyPlayer : WNetworkPlayer
{
    private MainMenu _mainMenu;
    private LobbyPanel _lobbyPanel;

    private void Start()
    {
        GameObject uiObject = GameObject.FindGameObjectWithTag("UIController");
        _mainMenu = uiObject.GetComponent<MainMenu>();
        _mainMenu.OpenPanel(MainMenu.LOBBY_PANEL);
        _lobbyPanel = uiObject.GetComponent<LobbyPanel>();
    }


    public override void OnStartClient()
    {   
        if (hasAuthority)
        {
            GameObject uiObject = GameObject.FindGameObjectWithTag("UIController");
            _mainMenu = uiObject.GetComponent<MainMenu>();
            _mainMenu.OpenPanel(MainMenu.LOBBY_PANEL);
            _lobbyPanel = uiObject.GetComponent<LobbyPanel>();


            CmdSetName(PlayFabUserData.Username);
            _lobbyPanel.SetLocalPlayer(this);
        }
        PlayerConnected();
    }


}
