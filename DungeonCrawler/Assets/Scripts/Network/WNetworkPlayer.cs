﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class WNetworkPlayer : NetworkBehaviour
{
    [SerializeField]
    [SyncVar]
    private string _playerName;
    public string PlayerName { get { return _playerName; } }

    [SerializeField]
    [SyncVar]
    private bool _ready;
    public bool Ready { get { return _ready; } }

    //For Lobby
    public static event Action<WNetworkPlayer> Event_PlayerConnectedToLobby;
    public static event Action<WNetworkPlayer> Event_PlayerDisconnectedFromLobby;


    [Command]
    protected void CmdSetName(string name)
    {
        _playerName = name;
    }

    public void SetReady(bool ready)
    {
        CmdSetReadyState(ready);
    }

    [Command]
    protected void CmdSetReadyState(bool ready)
    {
        _ready = ready;
    }

    public override void OnStartClient()
    {   
        if (hasAuthority)
        {
            CmdSetName(PlayFabUserData.Username);
        }
        PlayerConnected();
    }

    protected void PlayerConnected()
    {
        Event_PlayerConnectedToLobby?.Invoke(this);
    }

    public override void OnStopClient()
    {
        PlayerDisconnected();
    }

    protected void PlayerDisconnected()
    {
        Event_PlayerDisconnectedFromLobby?.Invoke(this);
    }
}
