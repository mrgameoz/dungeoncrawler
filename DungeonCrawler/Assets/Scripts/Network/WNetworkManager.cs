﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.SceneManagement;

public class WNetworkManager : NetworkManager
{
    [SerializeField]
    private string _mainMenuScene;
    [SerializeField]
    private string _trainingScene;

    private bool _bGameLoaded;

    [SerializeField]
    private List<NetworkConnection> _networkConnections = new List<NetworkConnection>();
    private List<WNetworkPlayer> _wNetworkPlayers = new List<WNetworkPlayer>();

    [Header("Level Details")]
    [SerializeField]
    private LevelManager _levelManager;

    [SerializeField]
    private GameObject _character;
    #region Lobby

    public override void OnServerConnect(NetworkConnection conn)
    {
        base.OnServerConnect(conn);
        _networkConnections.Add(conn);
    }

    public override void OnServerDisconnect(NetworkConnection conn)
    {
        _networkConnections.Remove(conn);
        _wNetworkPlayers.Remove(conn.identity.GetComponent<WNetworkPlayer>());
        base.OnServerDisconnect(conn);//object gets destroyed here
    }

    public override void OnClientConnect(NetworkConnection conn)
    {
        base.OnClientConnect(conn);
    }

    public override void OnClientDisconnect(NetworkConnection conn)
    {
        base.OnClientDisconnect(conn);
    }

    public override void OnServerAddPlayer(NetworkConnection conn)
    {
        Transform startPos = GetStartPosition();
        GameObject player = startPos != null
            ? Instantiate(playerPrefab, startPos.position, startPos.rotation)
            : Instantiate(playerPrefab);

        NetworkServer.AddPlayerForConnection(conn, player);

        _wNetworkPlayers.Add(player.GetComponent<WNetworkPlayer>());

    }

    #endregion

    #region ReadyCheck

    public override void LateUpdate()
    {
        base.LateUpdate();
        CheckIfPlayersAreReady();
    }

    private void CheckIfPlayersAreReady()
    {
        if(_bGameLoaded)
        {
            return;
        }
        int connectionsReady = 0;
        for (int i = 0; i < _wNetworkPlayers.Count; i++)
        {
            if(_wNetworkPlayers[i].Ready)
            {
                connectionsReady++;
            }
        }
        if(connectionsReady == maxConnections)
        {
            ServerChangeScene(_trainingScene);
            _bGameLoaded = true;
        }
    }

    #endregion

    #region Game
    public override void OnServerSceneChanged(string sceneName)
    {
        base.OnServerSceneChanged(sceneName);
        LoadLevelDetails();
    }

    private void SpawnPlayerCharacter(NetworkConnection conn)
    {
        Transform startPos = GetStartPosition();
        GameObject player = startPos != null
            ? Instantiate(_character, startPos.position, startPos.rotation)
            : Instantiate(_character);
        player.name = "Player " + NetworkServer.connections.Count;
        NetworkServer.ReplacePlayerForConnection(conn, player);
        //NetworkServer.AddPlayerForConnection(conn, player);
        Debug.Log($"{player.name} Added");
    }

    private void FindLevelManager()
    {
        _levelManager = GameObject.FindGameObjectWithTag("LevelManager").GetComponent<LevelManager>();
    }

    private void InitLevelManager()
    {
        _levelManager.Init();
    }

    private void LoadLevelDetails()
    {
        //initiate level
        FindLevelManager();
        InitLevelManager();

        //spawn player
        for (int i = 0; i < _networkConnections.Count; i++)
        {
            SpawnPlayerCharacter(_networkConnections[i]);
        }
    }

    #endregion
}
