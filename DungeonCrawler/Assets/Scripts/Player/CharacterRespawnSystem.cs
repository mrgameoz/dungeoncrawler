﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class CharacterRespawnSystem : NetworkBehaviour
{
    [SerializeField]
    private Transform _respawnPoint;
    private UnitMovementSystem _unitMovementSystem;
    private CharacterStatSystem _characterStatSystem;

    private void Awake()
    {
        _characterStatSystem = GetComponent<CharacterStatSystem>();
        _unitMovementSystem = GetComponent<UnitMovementSystem>();
    }

    // Start is called before the first frame update
    void Start()
    {
        _respawnPoint = GameObject.FindGameObjectWithTag("RespawnPoint").transform;
    }

    [Command]
    public void CmdRespawn()
    {
        Respawn();
    }

    [ServerCallback][ContextMenu("Respawn")]
    public void Respawn()
    {
        _unitMovementSystem.RepositionPlayer(_respawnPoint.position);
        StopCoroutine(RespawnAnimation());
        StartCoroutine(RespawnAnimation());
    }

    private IEnumerator RespawnAnimation()
    {

        yield return new WaitForSeconds(1f);
        _characterStatSystem.Revive();
    }
}
