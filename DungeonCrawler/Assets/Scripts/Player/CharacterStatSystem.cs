﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Mirror;

public class CharacterStatSystem : UnitStatSystem
{
    [Header("Champion Base Stats")]

    [SerializeField]
    [SyncVar]
    public BaseStats MaxStats;

    protected override void HealthChanged(int oldHealth, int newHealth)
    {
        //unitWorldSpaceUI.UpdateHealthBar((float)newHealth / (float)MaxStats.Health);
    }

    public override void OnStartServer()
    {
        MaxStats = new BaseStats(BaseStatsValue);
        base.OnStartServer();
    }

    protected override void InitStats()
    {
        base.InitStats();
        CurrentHealth = MaxStats.Health;
        CurrentMana = MaxStats.Mana;
        CurrentMovementSpeed = MaxStats.MovementSpeed;
    }

    public override void Revive()
    {
        UnitState = UnitState.Alive;
        CurrentHealth = MaxStats.Health;
        CurrentMana = MaxStats.Mana;
        UnitAnimationSystem.PlayDeadAnimation(false);
    }

    [ServerCallback]
    public override void ServerHealPlayer(int heal)
    {
        if (UnitState == UnitState.InCombat || UnitState == UnitState.Dead)
        {
            return;
        }
        CurrentHealth += heal;
        if (CurrentHealth > MaxStats.Health)
        {
            CurrentHealth = MaxStats.Health;
            StopHealthRegen();
        }
    }

    [ServerCallback]
    protected override void CheckIfHealthRegenIsRequired()
    {
        if (!HealthRegenTimer.Active && UnitState == UnitState.Alive && CurrentHealth < MaxStats.Health)
        {
            HealthRegenTimer.StartTimer(SecondsToRegenHealth, true);
        }
    }
}
