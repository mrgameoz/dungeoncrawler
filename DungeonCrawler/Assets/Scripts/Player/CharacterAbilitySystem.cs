﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using Mirror;
using UnityEngine.InputSystem;
public class CharacterAbilitySystem : UnitAbilitySystem
{
    protected DashSkills dashSkill;

    protected SpawnAbility _spawnComponent;
    protected DashBase dashBase;

    [SyncVar]
    protected int _selectedSkill = 0;//no skill is selected;
    public event Action<BaseSkills> EventSelectedAbility;
    public event Action EventInvalidLineOfSight;
    private bool _autoAttackEnabled;

    private PlayerInputControls _controls;

    protected override void Awake()
    {
        base.Awake();
        dashBase = GetComponent<DashBase>();
        _spawnComponent = GetComponent<SpawnAbility>();
        _controls = new PlayerInputControls();   
    }

    private void OnEnable()
    {
        CreateControls();
    }

    private void OnDisable()
    {
        RemoveControls();
    }

    protected override void Start()
    {
        base.Start();
        CreateControls();
    }

    [ServerCallback]
    protected override void ServerStart()
    {
        base.ServerStart();
        dashSkill = AbilityList[5] as DashSkills;
    }

    public override void SafeUpdate()
    {
        base.SafeUpdate();
        UpdateBasicAttack();
    }

    #region Server
    [ServerCallback]
    protected override void UpdateAbilitiesState()
    {
        base.UpdateAbilitiesState();
        AbilityOne_CD = AbilityList[0].PercentageRemaining();
        AbilityTwo_CD = AbilityList[1].PercentageRemaining();
        AbilityThree_CD = AbilityList[2].PercentageRemaining();
        AbilityFour_CD = AbilityList[3].PercentageRemaining();
        AbilityFive_CD = AbilityList[4].PercentageRemaining();
        AbilitySix_CD = AbilityList[5].PercentageRemaining();
    }

    [ServerCallback]
    protected void FireAtTarget()
    {
        CastSelectedAbility();
        //cancel next auto attack because this ability doesnt need follow up.
        //mouse abilities dont need follow up
        if (!AbilityList[_selectedSkill].AutoCastNextAbility)
        {
            DisableAutoAttack();
        }
        SelectedAbility(0);
    }

    [ServerCallback]
    private void UpdateBasicAttack()
    {
        if(_autoAttackEnabled && AnimationCooldown<=0)
        {
            FireAtTarget();
        }
    }

    [Command]
    public virtual void CmdAutoAttackEnabled()
    {
        _autoAttackEnabled = true;
    }

    [Command]
    public virtual void CmdAutoAttackDisabled()
    {
        DisableAutoAttack();
    }

    [ServerCallback]
    private void DisableAutoAttack()
    {
        _autoAttackEnabled = false;
    }

    [ServerCallback]
    public virtual void CastSelectedAbility()
    {
        if(AbilityList[_selectedSkill].AbilityTargetType ==AbilityTargetType.GroundTarget)
        {
            if(!TargetInLineOfSight)
            {
                return;
            }
        }
        AbilityFunctions[_selectedSkill]();
    }


    [Command]
    protected virtual void CmdSelectedAbility(int ability)
    {
        SelectedAbility(ability);
    }

    [Command]
    public override void CmdSetTargetPoint(Vector3 target)
    {
        TargetPoint = target;
    }

    [Server]
    protected virtual void SelectedAbility(int ability)
    {
        if (AbilityList[ability].Usable())
        {
            _selectedSkill = ability;
        }
        else
        {
            _selectedSkill = 0;//basic attack
        }
        RpcSelectedAbility(_selectedSkill);
    }

    [ServerCallback]
    protected override void CastAbilityOne()
    {
        if (!AbilityList[0].Usable())
        {
            return;
        }
        UnitAnimationSystem.TriggerBasicAttackAnimation();
        AnimationCooldown = AbilityList[0].SkillUsed();
        _spawnComponent.UseAbility(TargetPoint,AbilityList[0].Id, AbilityList[0].Damage);
    }


    protected override void CastAbilityTwo()
    {
        if (!AbilityList[1].Usable())
        {
            return;
        }

    }

    protected override void CastAbilityThree()
    {
        if (!AbilityList[2].Usable())
        {
            return;
        }

    }

    protected override void CastAbilityFour()
    {
        if (!AbilityList[3].Usable())
        {
            return;
        }

    }

    protected override void CastAbilityFive()
    {
        if (!AbilityList[4].Usable())
        {
            return;
        }

    }

    protected override void CastAbilitySix()
    {
        if (!AbilityList[5].Usable())
        {
            return;
        }
        AnimationCooldown = AbilityList[5].SkillUsed();
        dashBase.UseDashAbility(TargetPoint, dashSkill.DashRange, dashSkill.DashDuration, AbilityList[5].Id);
    }

    #endregion

    #region Client

    public virtual void FireAction_performed(InputAction.CallbackContext obj)
    {
        CmdAutoAttackEnabled();
    }

    public virtual void AbilityTwo_performed(InputAction.CallbackContext obj)
    {
        CmdSelectedAbility(1);
    }

    public virtual void AbilityThree_performed(InputAction.CallbackContext obj)
    {
        CmdSelectedAbility(2);
    }

    public virtual void AbilityFour_performed(InputAction.CallbackContext obj)
    {
        CmdSelectedAbility(3);
    }

    public virtual void AbilityFive_performed(InputAction.CallbackContext obj)
    {
        CmdSelectedAbility(4);
    }

    public virtual void AbilitySix_performed(InputAction.CallbackContext obj)
    {
        CmdSelectedAbility(5);
    }

    public void CancelAbility_performed(InputAction.CallbackContext obj)
    {
        if(_selectedSkill>0)
        {
            CmdSelectedAbility(0);
        }
        CmdAutoAttackDisabled();
    }

    [ClientRpc]
    private void RpcSelectedAbility(int newAbility)
    {
        if(!hasAuthority)
        {
            return;
        }
        EventSelectedAbility?.Invoke(AbilityList[newAbility]);
    }
    #endregion

    #region Controls
    private void CreateControls()
    {
        if (hasAuthority)
        {
            _controls.Player.Enable();
            _controls.Player.BasicAttack.performed += FireAction_performed;
            _controls.Player.BasicAttack.canceled += CancelAbility_performed;
            _controls.Player.Ability1.performed += AbilityTwo_performed;
            _controls.Player.Ability2.performed += AbilityThree_performed;
            _controls.Player.Ability3.performed += AbilityFour_performed;
            _controls.Player.Ability4.performed += AbilityFive_performed;
            _controls.Player.Ability5.performed += AbilitySix_performed;
            _controls.Player.Move.performed += CancelAbility_performed;

        }
    }

    private void RemoveControls()
    {
        if (hasAuthority)
        {
            _controls.Player.BasicAttack.performed -= FireAction_performed;
            _controls.Player.BasicAttack.canceled -= CancelAbility_performed;
            _controls.Player.Ability1.performed -= AbilityTwo_performed;
            _controls.Player.Ability2.performed -= AbilityThree_performed;
            _controls.Player.Ability3.performed -= AbilityFour_performed;
            _controls.Player.Ability4.performed -= AbilityFive_performed;
            _controls.Player.Ability5.performed -= AbilitySix_performed;
            _controls.Player.Move.performed -= CancelAbility_performed;
            _controls.Player.Disable();
        }
    }
    #endregion
}
