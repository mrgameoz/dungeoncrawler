﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class CharacterController : UnitController
{
    public PlayerTargetHandler playerTargetHandler;

    public static event Action<CharacterController> playerCreated;
    public static event Action<CharacterController> playerDestroyed;


    protected override void Awake()
    {
        base.Awake();
        playerTargetHandler = GetComponent<PlayerTargetHandler>();
    }

    protected override void Update()
    {
        if (UnitStatSystem.UnitState != UnitState.Dead)
        {
            playerTargetHandler.SafeUpdate();
            UnitStatSystem.SafeUpdate();
            if (UnitAbilitySystem != null)
            {
                UnitAbilitySystem.SafeUpdate();
            }
            UnitAnimationSystem.SafeUpdate();
            UnitMovementSystem.SafeUpdate();
        }
    }

    private void OnEnable()
    {
        playerTargetHandler.EventSetGameObjectAsTarget += UnitAbilitySystem.CmdSetTarget;
        //playerTargetHandler.EventSetGameObjectAsTarget += unitMovementSystem.CmdSetTarget;
        //playerTargetHandler.EventSetGameObjectAsTarget += unitLookAtRotation.CmdSetTarget;

        playerTargetHandler.EventSetVectorLookAtTarget += UnitAbilitySystem.CmdSetTargetPoint;
        playerTargetHandler.EventSetVectorPositionAsTarget += UnitMovementSystem.CmdMoveToPoint;
        playerTargetHandler.EventGroundTargetInLineOfSight += UnitAbilitySystem.CmdSetLineOfSightInfo;
    }

    private void OnDisable()
    {
        playerTargetHandler.EventSetGameObjectAsTarget -= UnitAbilitySystem.CmdSetTarget;
        //playerTargetHandler.EventSetGameObjectAsTarget -= unitMovementSystem.CmdSetTarget;
        //playerTargetHandler.EventSetGameObjectAsTarget -= unitLookAtRotation.CmdSetTarget;

        playerTargetHandler.EventSetVectorLookAtTarget -= UnitAbilitySystem.CmdSetTargetPoint;
        playerTargetHandler.EventSetVectorPositionAsTarget -= UnitMovementSystem.CmdMoveToPoint;
        playerTargetHandler.EventGroundTargetInLineOfSight -= UnitAbilitySystem.CmdSetLineOfSightInfo;
    }

    public override void OnStartServer()
    {
        playerCreated?.Invoke(this);
    }

    public override void OnStopServer()
    {
        playerDestroyed?.Invoke(this);
    }

    public override void OnStartClient()
    {
        if(!isClientOnly)
        {
            return;
        }
        playerCreated?.Invoke(this);
    }

    public override void OnStopClient()
    {
        if (!isClientOnly)
        {
            return;
        }
        playerDestroyed?.Invoke(this);
    }

}
