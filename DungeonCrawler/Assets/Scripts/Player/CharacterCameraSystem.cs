﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using Mirror;

public class CharacterCameraSystem : NetworkBehaviour
{
    private Transform _playerTransform;
    private CinemachineVirtualCamera _cinemachineVirtualCamera;

    // Start is called before the first frame update
    void Start()
    {
        _playerTransform = transform;
        if(hasAuthority)
        {
            _cinemachineVirtualCamera = GameObject.FindObjectOfType<CinemachineVirtualCamera>();
            _cinemachineVirtualCamera.Follow = _playerTransform;
        }

    }
}
