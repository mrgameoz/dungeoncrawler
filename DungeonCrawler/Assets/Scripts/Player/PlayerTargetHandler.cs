﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.InputSystem;

public class PlayerTargetHandler : NetworkBehaviour, IUnitTargetHandler
{

    private Camera _camera;
    [SerializeField]
    private LayerMask _movementLayerMask;
    [SerializeField]
    private LayerMask _targetLayerMask;
    [SerializeField]
    private LayerMask _groundTargetLayerMask;
    private UnitMovementSystem _unitMovement;
    private UnitLookAtRotation _unitRotation;

    private GameObject _currentTarget;
    private Vector3 _currentTargetPoint;
    public event Action<GameObject> EventSetGameObjectAsTarget;
    public event Action EventRemoveGameObjectAsTarget;
    public event Action<Vector3> EventSetVectorPositionAsTarget;
    public event Action<bool> EventGroundTargetInLineOfSight;
    public event Action<Vector3> EventSetVectorLookAtTarget;
    private bool _fireButtonPressed = false;
    private bool _moveButtonPressed = false;
    private PlayerInputControls _controls;
    //ground Target
    private GameObject _aoeCursor;
    [SerializeField]
    private LayerMask _lineOfSightMask;
    private void Awake()
    {
        _unitMovement = GetComponent<UnitMovementSystem>();
        _unitRotation = GetComponent<UnitLookAtRotation>();
        _controls = new PlayerInputControls();
    }

    private void OnEnable()
    {
        CreateControls();
    }

    private void CreateControls()
    {
        if (hasAuthority)
        {
            _controls.Player.Enable();
            _controls.Player.Move.performed += Move;
            _controls.Player.Move.canceled += MoveCancelled;
            _controls.Player.BasicAttack.performed += Target;
            _controls.Player.BasicAttack.canceled += TargetCancelled;
        }
    }

    private void RemoveControls()
    {
        if (hasAuthority)
        {
            _controls.Player.Move.performed -= Move;
            _controls.Player.Move.canceled -= MoveCancelled;
            _controls.Player.BasicAttack.performed -= Target;
            _controls.Player.BasicAttack.canceled -= TargetCancelled;
            _controls.Player.Disable();
        }
    }

    private void OnDisable()
    {
        RemoveControls();
    }

    // Start is called before the first frame update
    void Start()
    {
        if (hasAuthority)
        {
            CreateControls();
            _camera = Camera.main;
            _aoeCursor = GameObject.FindGameObjectWithTag("PlayerGroundTarget");
            _aoeCursor.SetActive(false);
        }
    }

    // Safe Update is called on controller Update() this update only runs on server
    public void SafeUpdate()
    {
        UpdateGroundTargetPos();
        UpdateFire();
        UpdateMove();
    }

    private void UpdateFire()
    {
        if (!_fireButtonPressed)
        {
            return;
        }
        DiscoverTargetToLook();
    }

    private void UpdateMove()
    {
        if(!_moveButtonPressed)
        {
            return;
        }
        DiscoverTargetToMove();
    }

    public void DiscoverTargetToMove()
    {
        if (!hasAuthority)
        {
            return;
        }
        Ray ray = _camera.ScreenPointToRay(Mouse.current.position.ReadValue());
        if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, _movementLayerMask))
        {
            //if (IsTargetable(hit.collider.gameObject))
            //{
            //    EventSetGameObjectAsTarget?.Invoke(hit.collider.gameObject);
            //    _currentTarget = hit.collider.gameObject;
            //}
            //else
            {
                EventSetVectorPositionAsTarget?.Invoke(hit.point);
                _currentTargetPoint = hit.point;
            }
        }
    }

    private bool IsInLineOfSight(Vector3 targetPos)
    {
        RaycastHit hit;
        Vector3 playerPos = transform.position;
        Vector3 dir = (targetPos - playerPos).normalized;
        Ray ray = new Ray(playerPos, dir);

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, _lineOfSightMask))
        {
            if (hit.transform.IsPlayerGroundTarget())
            {
                return true;
            }
        }
        return false;
    }

    public void DiscoverTargetToLook()
    {
        if (!hasAuthority)
        {
            return;
        }
        Ray ray = _camera.ScreenPointToRay(Mouse.current.position.ReadValue());
        if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, _targetLayerMask))
        {
            GameObject other = hit.collider.gameObject;
            if (other.GameObjectIsTargetable())
            {
                EventSetGameObjectAsTarget?.Invoke(other);
                _currentTarget = other;
            }
            else
            {
                EventRemoveGameObjectAsTarget?.Invoke();
            }
            _unitRotation.CmdLookAtDirection(hit.point);
            EventSetVectorLookAtTarget?.Invoke(hit.point);
            _currentTargetPoint = hit.point;
        }
    }

    private void UpdateGroundTargetPos()
    {
        if(!hasAuthority)
        {
            return;
        }

        Ray ray = _camera.ScreenPointToRay(Mouse.current.position.ReadValue());
        if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, _groundTargetLayerMask))
        {

            _aoeCursor.transform.rotation = Quaternion.FromToRotation(Vector3.up, hit.normal);
            _aoeCursor.transform.position = hit.point;
            bool isInLineOfSight = IsInLineOfSight(hit.point);
            if(isInLineOfSight)
            {
                isInLineOfSight = !hit.transform.TransformIsUnTargetable();
            }
            EventGroundTargetInLineOfSight?.Invoke(isInLineOfSight);
        }
    }

    public virtual void Move(InputAction.CallbackContext obj)
    {
        StopFiring();
        _moveButtonPressed = true;
    }

    public virtual void MoveCancelled(InputAction.CallbackContext obj)
    {
        _moveButtonPressed = false;
    }

    public virtual void Target(InputAction.CallbackContext obj)
    {
        StopMovement();
        _fireButtonPressed = true;
    }

    public virtual void TargetCancelled(InputAction.CallbackContext obj)
    {
        StopFiring();
    }

    private void StopFiring()
    {

        _fireButtonPressed = false;
    }

    private void StopMovement()
    {

        _moveButtonPressed = false;
        _unitMovement.CmdStopMovement();
    }

    private bool IsTargetable(GameObject gameObject)
    {
        bool targetable = false;
        bool foundTarget = gameObject.TryGetComponent<Targetable>(out Targetable target);
        if(foundTarget)
        {
            targetable = !target.hasAuthority;
        }
        return targetable;
    }

}
