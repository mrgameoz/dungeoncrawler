﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using Mirror.Discovery;

public class MultiplayerPanel : MonoBehaviour
{
    [SerializeField]
    private WNetworkManager _networkRoom;

    //Lobby
    private readonly Dictionary<long, ServerResponse> _discoveredServers = new Dictionary<long, ServerResponse>();
    private readonly List<long> _serverIDs = new List<long>();

    [SerializeField]
    private WNetworkDiscovery _networkDiscovery;

    private Timer _serverFindTimer;
    private Timer _serverAdvertiseTimer;

    public void EnableRoomManger()
    {
        _networkRoom.gameObject.SetActive(true);
    }

    private void OnEnable()
    {
        _networkDiscovery.OnServerFound.AddListener(OnDiscoveredServer);


        _serverFindTimer = new Timer(FindServers);
        _serverFindTimer.StartTimer(1f,true);

        _serverAdvertiseTimer = new Timer(AdvertiseServer);

    }

    private void OnDisable()
    {
        //_networkDiscovery.OnWServerFound -= OnDiscoveredServer;
        _networkDiscovery.OnServerFound.RemoveListener(OnDiscoveredServer);

        _serverAdvertiseTimer.StopTimer();
        _serverFindTimer.StopTimer();
    }

    private void Update()
    {
        _serverAdvertiseTimer.Update();
        _serverFindTimer.Update();
    }

    public void HostGame()
    {
        _discoveredServers.Clear();
        NetworkManager.singleton.StartHost();
        AdvertiseServer();
        _serverAdvertiseTimer.StartTimer(1, true);
    }

    private void AdvertiseServer()
    {
        _networkDiscovery.AdvertiseServer();
    }

    public void JoinGame()
    {
        if(_serverIDs.Count>0)
        {
            Connect(_discoveredServers[_serverIDs[0]]);
        }
    }

    private void FindServers()
    {
        _serverIDs.Clear();
        _discoveredServers.Clear();
        _networkDiscovery.StartDiscovery();
    }

    void Connect(ServerResponse info)
    {
        Debug.Log("Connecting To " + info.serverId + " Uri " + info.uri);
        NetworkManager.singleton.StartClient(info.uri);
    }

    public void OnDiscoveredServer(ServerResponse info)
    {
        // Note that you can check the versioning to decide if you can connect to the server or not using this method
        _serverIDs.Add(info.serverId);
        _discoveredServers[info.serverId] = info;
        Debug.Log("Found a new server");
    }

}
