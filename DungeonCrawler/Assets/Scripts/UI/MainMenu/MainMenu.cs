﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MainMenu : PanelManager
{
    //panel indexes
    public const int LOGIN_PANEL = 0;
    public const int PROFILE_PANEL = 1;
    public const int MULTIPLAYER_PANEL = 2;
    public const int LOBBY_PANEL = 3;

    [SerializeField]
    private TMP_Text _usernameText;

    private void Start()
    {
        CurrentPanel = LOGIN_PANEL;
        CheckForSessionTicket();
    }

    private void Update()
    {
        CheckForSessionTicket();
    }

    private void CheckForSessionTicket()
    {
        if(!UiPanels[LOGIN_PANEL].activeSelf)
        {
            return;
        }
        //if session ticket is available return
        if (PlayFabUserData.SessionTicket == null || PlayFabUserData.SessionTicket == string.Empty)
        {
            return;
        }
        UiPanels[LOGIN_PANEL].SetActive(false);
        GetUsername();
        OpenPanel(PROFILE_PANEL);
    }

    private void GetUsername()
    {
        if (PlayFabUserData.Username != null && PlayFabUserData.Username != string.Empty)
        {
            Debug.Log("No Action");
            SetUsername();
        }
        else
        {
            PlayFabUserData.GetUsername(SetUsername);
        }
    }

    private void SetUsername()
    {
        Debug.Log("Action");
        _usernameText.text = PlayFabUserData.Username;
    }





}

