﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LobbyPanel : MonoBehaviour
{
    [SerializeField]
    private List<LobbyPlayerFrame> _lobbyPlayerFrames = new List<LobbyPlayerFrame>();
    private List<WNetworkPlayer> _playersConnected = new List<WNetworkPlayer>();

    private WNetworkLobbyPlayer _localPlayer;
    private bool _ready = false;

    [SerializeField]
    private Button _readyButton;

    private void OnEnable()
    {
        WNetworkPlayer.Event_PlayerConnectedToLobby += HandleOnPlayerConnected;
        WNetworkPlayer.Event_PlayerDisconnectedFromLobby += HandleOnPlayerDisconnected;
    }

    private void OnDisable()
    {
        WNetworkPlayer.Event_PlayerConnectedToLobby -= HandleOnPlayerConnected;
        WNetworkPlayer.Event_PlayerDisconnectedFromLobby -= HandleOnPlayerDisconnected;
    }

    private void Update()
    {
        UpdateLobbyUI();
    }

    private void UpdateLobbyUI()
    {
        for (int i = 0; i < _lobbyPlayerFrames.Count; i++)
        {
            if(i < _playersConnected.Count)
            {
                _lobbyPlayerFrames[i].SetName(_playersConnected[i].PlayerName);
                _lobbyPlayerFrames[i].SetReadyOrNotReady(_playersConnected[i].Ready);
            }
            else
            {
                _lobbyPlayerFrames[i].SetName("");
                _lobbyPlayerFrames[i].SetReadyOrNotReady(false);
            }
        }
    }

    public void SetReady()
    {
        _ready = true;
        _localPlayer.SetReady(_ready);
        _readyButton.interactable = false;
    }

    public void SetLocalPlayer(WNetworkLobbyPlayer player)
    {
        _localPlayer = player;
    }

    private void HandleOnPlayerConnected(WNetworkPlayer player)
    {
        _playersConnected.Add(player);
    }

    private void HandleOnPlayerDisconnected(WNetworkPlayer player)
    {
        _playersConnected.Remove(player);
    }
}
