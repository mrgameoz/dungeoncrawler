﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LobbyPlayerFrame : MonoBehaviour
{
    [SerializeField]
    private TMP_Text _nameText;
    [SerializeField]
    private Image _characterImage;
    [SerializeField]
    private Image _readyImage;
    [SerializeField]
    private Color _readyColor;
    [SerializeField]
    private Color _notReadyColor;
    private bool _ready;

    public void SetName(string name)
    {
        _nameText.text = name;
    }
    
    public void SetReadyOrNotReady(bool ready)
    {
        _ready = ready;
        if(_ready)
        {
            _readyImage.color = _readyColor;
        }
        else
        {
            _readyImage.color = _notReadyColor;
        }
    }

    public void SetCharacterImage()
    {

    }
}
