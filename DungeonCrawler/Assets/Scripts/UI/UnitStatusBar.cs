﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UnitStatusBar : MonoBehaviour
{
    [SerializeField]
    private Image _healthBar;
    [SerializeField]
    private Image _manahBar;
    [SerializeField]
    private TMP_Text _nameText
        ;
    [SerializeField]
    private TMP_Text _healthText;
    public void UpdateHealthBar(float percentage)
    {
        _healthBar.fillAmount = percentage;
    }

    public void UpdateManaBar(float percentage)
    {
        _manahBar.fillAmount = percentage;
    }

    public void UpdateName(string name)
    {
        _nameText.text = name;
    }

    public void UpdateHealth(string health)
    {
        _healthText.text = health;
    }
}
