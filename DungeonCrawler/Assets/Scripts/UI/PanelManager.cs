﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PanelManager : MonoBehaviour
{
    protected int CurrentPanel;

    [SerializeField]
    protected List<GameObject> UiPanels = new List<GameObject>();

    private Stack<GameObject> _activePanels = new Stack<GameObject>();

    public void OpenPanel(int panel)
    {
        CurrentPanel = panel;
        AddToStack(CurrentPanel);
    }

    public void AddToStack(int index)
    {
        UiPanels[index].SetActive(true);
        _activePanels.Push(UiPanels[index]);
    }

    public void RemoveFromStack()
    {
        _activePanels.Pop().SetActive(false);
    }
}
