﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerWorldViewUI : MonoBehaviour
{
    [SerializeField]
    private RectTransform _rootCanvasRect;

    [SerializeField]
    private GameObject _playerHealthBarPrefab;
    [SerializeField]
    private Transform _healthBarParentTransform;

    [SerializeField]
    private List<CharacterStatSystem> _playersConnected = new List<CharacterStatSystem>();

    private List<UnitStatusBar> _playerStatusBars = new List<UnitStatusBar>();

    [SerializeField]
    private Vector3 _healthBarOffset;
    private Camera _camera;

    private void Awake()
    {
        _camera = Camera.main;
        CharacterController.playerCreated += PlayerCreated;
        CharacterController.playerDestroyed += PlayerDestroyed;
    }

    private void PlayerCreated(CharacterController player)
    {
        _playersConnected.Add(player.GetComponent<CharacterStatSystem>());
        CreateHealthBarForPlayer(player.name);
    }

    private void PlayerDestroyed(CharacterController player)
    {
        _playersConnected.Remove(player.GetComponent<CharacterStatSystem>());
        RemoveHealthBar();
    }

    private void OnDestroy()
    {
        CharacterController.playerCreated -= PlayerCreated;
        CharacterController.playerDestroyed -= PlayerDestroyed;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        DrawHealthBarsForPlayers();
    }

    private void DrawHealthBarsForPlayers()
    {
        for (int i = 0; i < _playersConnected.Count; i++)
        {
            Vector3 screenPos = _camera.WorldToViewportPoint(_playersConnected[i].transform.position + _healthBarOffset);
            screenPos.x *= _rootCanvasRect.rect.width;
            screenPos.y *= _rootCanvasRect.rect.height;

            _playerStatusBars[i].GetComponent<RectTransform>().anchoredPosition = screenPos;

            float healthPercentage = (float)_playersConnected[i].CurrentHealth / (float)_playersConnected[i].MaxStats.Health;
            _playerStatusBars[i].UpdateHealthBar(healthPercentage);

            float manaPercentage = (float)_playersConnected[i].CurrentMana / (float)_playersConnected[i].MaxStats.Mana;
            _playerStatusBars[i].UpdateManaBar(manaPercentage);
        }
    }

    private void CreateHealthBarForPlayer(string playerName)
    {
        GameObject go = Instantiate(_playerHealthBarPrefab, Vector3.zero, Quaternion.identity ,_healthBarParentTransform);
        go.name = playerName;
        _playerStatusBars.Add(go.GetComponent<UnitStatusBar>());
    }

    private void RemoveHealthBar()
    {
        if(_playerStatusBars.Count>0)
        {
            GameObject go = _playerStatusBars[_playerStatusBars.Count - 1].gameObject;
            _playerStatusBars.RemoveAt(_playerStatusBars.Count - 1);
            go.SetActive(false);
        }
    }
}
