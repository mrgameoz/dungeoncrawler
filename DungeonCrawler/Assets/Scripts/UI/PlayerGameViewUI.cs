﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using TMPro;

public class PlayerGameViewUI : MonoBehaviour
{
    [SerializeField]
    private PlayerInformation _playerInformation;

    private Transform _playerTransform;
    private CharacterController _characterController;
    private CharacterStatSystem _characterStatSystem;
    private CharacterAbilitySystem _characterAbilitySystem;

    //Target UI
    private UnitStatSystem _targetEnemy;
    private CharacterStatSystem _targetAlly;
    [SerializeField]
    private UnitStatusBar _enemyTargetStatusBar;
    [SerializeField]
    private UnitStatusBar _allyTargetStatusBar;

    //Ability Target Cursor
    private BaseSkills _selectedAbility;
    private AbilityTargetType _selectedAbilityTargetType;
    [Header("Player Target")]
    [SerializeField]
    private Image _targetCursor;
    [SerializeField]
    private Image _abilityCursor;
    [SerializeField]
    private GameObject _aoeCursor;
    [SerializeField]
    private Renderer _aoeCursorRenderer;
    private Image _currentCursor;
    private Camera _camera;
    [SerializeField]
    private LayerMask _layerMask;

    //color
    [SerializeField]
    private Color _color_Targetable;
    [SerializeField]
    private Color _color_Untargetgable;

    [Header("Player Champion Panel")]
    //Player HealthBar
    [SerializeField]
    private Image _healthBar;
    [SerializeField]
    private TMP_Text _healthBarText;
    //Ability UI
    [SerializeField]
    private Image[] _abilityIcons;

    //Warning Messages
    [SerializeField]
    private GameObject _warningLineOfSight;


    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
        _currentCursor = _targetCursor;
        _camera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        FindClientPlayer();
        UpdateTargetInformation();
        UpdateTargetCursorPosition();
        UpdateAbilityTimer();
        UpdateHealthBar();
    }

    private void FindClientPlayer()
    {
        if(_characterController==null)
        {
            for (int i = 0; i < _playerInformation.PlayersConnected.Count; i++)
            {
                if(_playerInformation.PlayersConnected[i].hasAuthority)
                {
                    _characterController = _playerInformation.PlayersConnected[i];
                    _characterStatSystem = _characterController.UnitStatSystem as CharacterStatSystem;
                    _playerTransform = _characterController.transform;
                    _characterAbilitySystem = _playerInformation.PlayersConnected[i].UnitAbilitySystem as CharacterAbilitySystem;
                    _characterAbilitySystem.EventSelectedAbility += UpdateSelectedAbility;
                    _characterAbilitySystem.EventInvalidLineOfSight += InvalidLineOfSight;
                    _characterController.playerTargetHandler.EventSetGameObjectAsTarget += SetTarget;
                    _characterController.playerTargetHandler.EventGroundTargetInLineOfSight += SetGroundTargetInfo;
                    _characterController.playerTargetHandler.EventRemoveGameObjectAsTarget += RemoveTarget;
                    break;
                }
            }
        }

    }

    private void SetTarget(GameObject target)
    {
        if (target.gameObject.CompareTag("Player"))
        {
            _targetAlly = target.GetComponent<CharacterStatSystem>();
            _enemyTargetStatusBar.gameObject.SetActive(false);
            _allyTargetStatusBar.gameObject.SetActive(true);
        }
        else
        {
            _targetEnemy = target.GetComponent<UnitStatSystem>();
            _allyTargetStatusBar.gameObject.SetActive(false);
            _enemyTargetStatusBar.gameObject.SetActive(true);
        }
    }

    private void RemoveTarget()
    {
        if (_targetAlly != null)
        {
            _targetAlly = null;
            _allyTargetStatusBar.gameObject.SetActive(false);
        }
        if (_targetEnemy != null)
        {
            _targetEnemy = null;
            _enemyTargetStatusBar.gameObject.SetActive(false);
        }
    }


    private void UpdateTargetInformation()
    {
        if(_targetAlly!=null)
        {
            _allyTargetStatusBar.UpdateName(_targetAlly.name);
            _allyTargetStatusBar.UpdateHealth(_targetAlly.CurrentHealth.ToString());
            _allyTargetStatusBar.UpdateHealthBar((float)_targetAlly.CurrentHealth / (float)_targetAlly.MaxStats.Health);
            _allyTargetStatusBar.UpdateManaBar((float)_targetAlly.CurrentMana / (float)_targetAlly.MaxStats.Mana);
        }
        if(_targetEnemy!=null)
        {
            _enemyTargetStatusBar.UpdateName(_targetEnemy.name);
            _enemyTargetStatusBar.UpdateHealth(_targetEnemy.CurrentHealth.ToString());
            _enemyTargetStatusBar.UpdateHealthBar((float)_targetEnemy.CurrentHealth / (float)_targetEnemy.BaseStatsValue.Health);
            _enemyTargetStatusBar.UpdateManaBar((float)_targetEnemy.CurrentMana / (float)_targetEnemy.BaseStatsValue.Mana);
        }

    }

    private void UpdateSelectedAbility(BaseSkills ability)
    {
        _selectedAbility = ability;
        _selectedAbilityTargetType = ability.AbilityTargetType;
        switch (_selectedAbilityTargetType)
        {
            case AbilityTargetType.NoTarget:
            case AbilityTargetType.SelfTarget:
                {
                    _aoeCursor.SetActive(false);
                    _abilityCursor.gameObject.SetActive(false);
                    _targetCursor.gameObject.SetActive(true);
                    _currentCursor = _targetCursor;
                    break;
                }
            case AbilityTargetType.AllyTarget:
                {
                    _aoeCursor.SetActive(false);
                    _targetCursor.gameObject.SetActive(false);
                    _abilityCursor.gameObject.SetActive(true);
                    _currentCursor = _abilityCursor;
                    break;
                }
            case AbilityTargetType.GroundTarget:
                {
                    _aoeCursor.SetActive(true);
                    _aoeCursor.transform.localScale = new Vector3(_selectedAbility.Radius, 0.1f, _selectedAbility.Radius);
                    _targetCursor.gameObject.SetActive(false);
                    _abilityCursor.gameObject.SetActive(false);
                    break;
                }
            default:
                break;
        }
    }

    private void UpdateAbilityTimer()
    {
        if(_characterAbilitySystem==null)
        {
            return;
        }

        _abilityIcons[0].fillAmount = _characterAbilitySystem.AbilityOne_CD;
        _abilityIcons[1].fillAmount = _characterAbilitySystem.AbilityTwo_CD;
        _abilityIcons[2].fillAmount = _characterAbilitySystem.AbilityThree_CD;
        _abilityIcons[3].fillAmount = _characterAbilitySystem.AbilityFour_CD;
        _abilityIcons[4].fillAmount = _characterAbilitySystem.AbilityFive_CD;
        _abilityIcons[5].fillAmount = _characterAbilitySystem.AbilitySix_CD;
    }

    private void UpdateHealthBar()
    {
        if (_characterStatSystem == null)
        {
            return;
        }
        _healthBarText.text = _characterStatSystem.CurrentHealth.ToString();
        _healthBar.fillAmount = (float)_characterStatSystem.CurrentHealth / (float)_characterStatSystem.BaseStatsValue.Health;
    }

    private void UpdateTargetCursorPosition()
    {
        Vector3 mousePosition = Mouse.current.position.ReadValue();

        if (_selectedAbilityTargetType!=AbilityTargetType.GroundTarget)
        {
            _currentCursor.rectTransform.position = mousePosition;
        }

    }

    private void SetGroundTargetInfo(bool isInLineOfSight)
    {
        if (isInLineOfSight)
        {
            MaterialPropertyBlock propertyBlock = new MaterialPropertyBlock();
            propertyBlock.SetColor("_BaseColor", _color_Targetable);
            _aoeCursorRenderer.SetPropertyBlock(propertyBlock);
        }
        else
        {
            MaterialPropertyBlock propertyBlock = new MaterialPropertyBlock();
            propertyBlock.SetColor("_BaseColor", _color_Untargetgable);
            _aoeCursorRenderer.SetPropertyBlock(propertyBlock);
        }
    }

    private void InvalidLineOfSight()
    {
        _warningLineOfSight.SetActive(true);
        StopCoroutine(LineOfSightMessage());
        StartCoroutine(LineOfSightMessage());
    }

    private IEnumerator LineOfSightMessage()
    {
        yield return new WaitForSeconds(2);
        _warningLineOfSight.SetActive(false);
    }


    private void OnDestroy()
    {
        if(_characterController!=null)
        {
            _characterController.playerTargetHandler.EventSetGameObjectAsTarget -= SetTarget;
            _characterController.playerTargetHandler.EventRemoveGameObjectAsTarget -= RemoveTarget;
            _characterController.playerTargetHandler.EventGroundTargetInLineOfSight -= SetGroundTargetInfo;
        }
        if (_characterAbilitySystem != null)
        {
            _characterAbilitySystem.EventSelectedAbility -= UpdateSelectedAbility;
            _characterAbilitySystem.EventInvalidLineOfSight -= InvalidLineOfSight;
        }
    }

}
