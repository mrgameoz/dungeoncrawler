﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public static class ExtensionMethods
{
    public static bool GameObjectIsObstacle(this GameObject gameObject)
    {
        bool obstacle = false;
        if(gameObject.CompareTag("Obstacle") || gameObject.CompareTag("Ground"))
        {
            obstacle = true;
        }
        return obstacle;
    }

    public static bool TransformIsUnTargetable(this Transform transform)
    {
        bool obstacle = false;
        if (transform.CompareTag("Obstacle"))
        {
            obstacle = true;
        }
        return obstacle;
    }

    public static bool GameObjectIsUnTargetable(this GameObject gameObject)
    {
        bool obstacle = false;
        if (gameObject.CompareTag("Obstacle"))
        {
            obstacle = true;
        }
        return obstacle;
    }

    public static bool GameObjectIsTargetable(this GameObject gameObject)
    {
        bool targetable = false;
        if (gameObject.CompareTag("Player") || gameObject.CompareTag("Enemy"))
        {
            targetable = true;
        }
        return targetable;
    }

    public static bool GameObjectIsPlayer(this GameObject gameObject)
    {
        bool targetable = false;
        if (gameObject.CompareTag("Player"))
        {
            targetable = true;
        }
        return targetable;
    }

    public static bool TransformIsPlayer(this Transform transform)
    {
        bool targetable = false;
        if (transform.CompareTag("Player"))
        {
            targetable = true;
        }
        return targetable;
    }

    public static bool GameObjectIsEnemy(this GameObject gameObject)
    {
        bool targetable = false;
        if (gameObject.CompareTag("Enemy"))
        {
            targetable = true;
        }
        return targetable;
    }

    public static bool TransformIsEnemy(this Transform transform)
    {
        bool targetable = false;
        if (transform.CompareTag("Enemy"))
        {
            targetable = true;
        }
        return targetable;
    }

    public static bool IsPathReachable(this NavMeshAgent navMeshAgent,Vector3 destination)
    {
        NavMeshPath path = new NavMeshPath();
        bool pathIsValid = false;
        if (navMeshAgent.CalculatePath(destination, path))
        {
            pathIsValid = path.status == NavMeshPathStatus.PathComplete;
        }
        return pathIsValid;
    }

    public static bool IsPlayerGroundTarget(this GameObject gameObject)
    {
        return gameObject.CompareTag("PlayerGroundTarget");
    }

    public static bool IsPlayerGroundTarget(this Transform transform)
    {
        return transform.CompareTag("PlayerGroundTarget");
    }
}
