﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class VectorUtil
{
    /// Return if it is target point is in range or not
    public static bool IsInRange(Vector3 vectorA, Vector3 vectorB, float range)
    {
        if ((vectorB - vectorA).sqrMagnitude <= range * range)
        {
            return true;
        }
        return false;
    }


    public static float SqrMagnitude(Vector3 vectorA, Vector3 vectorB)
    {
        return (vectorB - vectorA).sqrMagnitude;
    }

    /// <summary>
    /// Return if it is distance is in range or not
    /// </summary>
    /// <param name="distance">distance between to points</param>
    /// <param name="range">range to compare with distance</param>
    /// <returns></returns>
    public static bool IsInRange(float distance, float range)
    {
        return distance * distance <= range * range;
    }
}
