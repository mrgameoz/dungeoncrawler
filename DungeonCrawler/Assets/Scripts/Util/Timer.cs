﻿using System;
using UnityEngine;

public class Timer
{
    private float _currentTime;
    private float _delay;
    private bool _repeat;

    private Action Handle_TimerCompleted;

    public bool Active { private set; get; }

    public Timer(Action handler)
    {
        Handle_TimerCompleted = handler;
    }

    public void StartTimer(float delay, bool repeat = false)
    {
        _currentTime = 0;
        _delay = delay;
        _repeat = repeat;
        Active = true;
    }

    public void Update()
    {
        if(Active)
        {
            _currentTime += Time.deltaTime;
            if(_currentTime>=_delay)
            {
                _currentTime -= _delay;
                Handle_TimerCompleted();
                if(!_repeat)
                {
                    StopTimer(false);
                }
            }
        }
    }

    public void StopTimer(bool invokeHandler = false)
    {
        if(invokeHandler)
        {
            Handle_TimerCompleted();
        }
        Active = false;
    }
}
