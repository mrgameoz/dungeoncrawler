﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="EnemyAI",menuName ="AI/Enemy",order = 0)]
public class EnemyData : ScriptableObject
{
    public int Id;
    public string EnemyName;
    public float ObserveRange;
    public float ChaseRange;
    public float AttackRange;
    public float FleeRange;
}

public enum EnemyAttackType { Ranged, Meele, Mixed };
public enum EnemyType { Normal, Commander, Veteran, Boss }

