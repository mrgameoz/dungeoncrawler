﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DashSkill", menuName = "Skills/DashSkill", order = 2)]
public class DashSkills : BaseSkills
{
    public float DashRange;
    public float DashDuration;
}
