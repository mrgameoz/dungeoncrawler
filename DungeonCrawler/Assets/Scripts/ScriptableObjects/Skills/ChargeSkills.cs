﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ChargeSkill", menuName = "Skills/ChargeSkill", order = 1)]
public class ChargeSkills : BaseSkills
{
    public int ChargesCount;
    public int ChargesRemaining;
    public float ChargeCooldown;

    public override bool Usable()
    {
        Debug.Log(CoolDownRemaining+" Charge " + ChargesRemaining);
        return CoolDownRemaining == 0 && ChargesRemaining > 0;
    }

    public override float SkillUsed()
    {
        ChargesRemaining--;
        if (ChargesRemaining == 0)//if zero charges are left reset ability cooldown
        {
            CoolDownRemaining = Cooldown;
        }
        else//use charge cooldown so player can cast next charge quickly
        {
            CoolDownRemaining = ChargeCooldown;
        }
        return AnimationCooldown;
    }

    public override void AbilityUpdate(float deltaTime)
    {
        CoolDownRemaining -= deltaTime;
        if (CoolDownRemaining <= 0)
        {
            CoolDownRemaining = 0;
            if(ChargesRemaining==0)
            {
                Debug.Log("Charge Reset");
                ChargesRemaining = ChargesCount;
            }
        }
    }

    public override float PercentageRemaining()
    {
        if(ChargesRemaining>0)
        {
            return CoolDownRemaining / ChargeCooldown;
        }
        return CoolDownRemaining / Cooldown;
    }

}
