﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="AutoAttackSkill",menuName ="Skills/Skill",order = 0)]
public class BaseSkills : ScriptableObject
{
    public int Id;
    public string Name;
    public AbilityRange AbilityRange;
    public AbilityType AbilityType;
    public AbilityTargetType AbilityTargetType;

    public int Damage;
    public float Range;
    public float Cooldown;
    public float Radius;
    public float AnimationCooldown; //to make sure animation finishes before unit can cast other skills
    public float GlobalCooldown; //affects all skills
    public float CoolDownRemaining;
    public bool AutoCastNextAbility = false; // allow to to cast auto attack after casting a ability, because player can hold mouse button to fire.
    public virtual void AbilityUpdate(float deltaTime)
    {
        CoolDownRemaining -= deltaTime;
        if (CoolDownRemaining<=0)
        {
            CoolDownRemaining = 0;
        }
    }

    public virtual float SkillUsed()
    {
        CoolDownRemaining = Cooldown;
        return AnimationCooldown;
    }

    public virtual float PercentageRemaining()
    {
        return CoolDownRemaining / Cooldown;
    }

    public virtual bool Usable()
    {
        return CoolDownRemaining == 0;
    }
}

public enum AbilityType { AutoAttack, AttackDamage, MagicDamage, Healing, Buff, Debuff, DamageOverTime, Mark, Dash, Blink };
public enum AbilityTargetType { NoTarget, SelfTarget, AllyTarget, EnemyTarget, GroundTarget };
public enum AbilityRange{ Melee, Ranged };

