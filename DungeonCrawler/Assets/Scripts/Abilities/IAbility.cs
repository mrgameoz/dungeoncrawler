﻿using System.Collections;
using UnityEngine;

public interface IAbility 
{
    bool UseSkill();

    void UseSkill(Vector3 endPos);

    bool UseSkill(GameObject target);

}
