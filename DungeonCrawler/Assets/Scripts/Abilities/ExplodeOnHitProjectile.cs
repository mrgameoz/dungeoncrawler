﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class ExplodeOnHitProjectile : ProjectileBase
{
    [SerializeField]
    private float _explosionRadius;

    [ServerCallback]
    protected override void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.GameObjectIsObstacle())
        {
            RpcDisableObject();
        }

        if (other.gameObject.CompareTag("Enemy"))
        {
            if (other.gameObject.TryGetComponent<UnitStatSystem>(out UnitStatSystem target))
            {
                InitateExplosion();
            }
        }
    }

    protected virtual void InitateExplosion()
    {
        Collider[] affectedObjects = Physics.OverlapSphere(transform.position, _explosionRadius);
        for (int i = 0; i < affectedObjects.Length; i++)
        {
            GameObject affectedGameObject = affectedObjects[i].gameObject;
            if (/*affectedGameObject.CompareTag("Enemy") &&*/
                affectedGameObject.TryGetComponent<UnitStatSystem>(out UnitStatSystem target))
                if (affectedGameObject.gameObject.CompareTag("Enemy"))
                {
                    Debug.Log("Explosion!!");
                    target.ServerTakeDamage(Damage);
                }
        }
        if (!_passThroughEnemies)
        {
            RpcDisableObject();
        }
    }
}
