﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Mirror;
using UnityEngine.AI;
public class JudyAbilitySystem : CharacterAbilitySystem
{
    //[SerializeField]
    //private StatModifier _abilityOne_StatModifer;
    private JudySpawnAbility _judyProjectileComponent;
    private NavMeshAgent _navMeshAgent;

    //shooting VFX
    [SerializeField]
    private ParticleSystem _muzzleFire;

    //RenderChange
    private string _alphaClipValue = "AlphaClipValue";
    [SerializeField]
    private Renderer _renderer;
    protected override void Awake()
    {
        base.Awake();
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _judyProjectileComponent = (_spawnComponent as JudySpawnAbility);
    }

    protected override void CastAbilityOne()
    {
        if (!AbilityList[0].Usable())
        {
            return;
        }
        UnitAnimationSystem.TriggerBasicAttackAnimation();
        AnimationCooldown = AbilityList[0].SkillUsed();
        RpcPlayMuzzleFire();
        _spawnComponent.UseAbility(TargetPoint, AbilityList[0].Id,AbilityList[0].Damage);
    }

    protected override void CastAbilityTwo()
    {
        if (!AbilityList[1].Usable())
        {
            return;
        }
        UnitAnimationSystem.TriggerBasicAttackAnimation();
        AnimationCooldown = AbilityList[1].SkillUsed();
        RpcPlayMuzzleFire();
        _judyProjectileComponent.UseTripleShotSkill(TargetPoint, AbilityList[1].Id, AbilityList[1].Damage);

        //UnitStatSystem.AddStatModifier(_abilityOne_StatModifer);
        Debug.Log("Ability Name " + AbilityList[1].name);
    }

    protected override void CastAbilityThree()
    {
        if (!AbilityList[2].Usable())
        {
            return;
        }
        AnimationCooldown = AbilityList[2].SkillUsed();
        _judyProjectileComponent.UseShadowLotus(TargetPoint, AbilityList[2].Id, AbilityList[2].Damage);
        Debug.Log("Ability Name " + AbilityList[2].name);
    }

    protected override void CastAbilityFour()
    {
        if (!AbilityList[3].Usable())
        {
            return;
        }
        if (!_navMeshAgent.IsPathReachable(TargetPoint))
        {
            return;
        }
        UnitMovementSystem.RepositionPlayer(TargetPoint);
        _judyProjectileComponent.UseShadowStep(TargetPoint, AbilityList[3].Id);
        AnimationCooldown = AbilityList[3].SkillUsed();
        RpcIniateLeapEffect();
        Debug.Log("Ability Name " + AbilityList[3].name);
    }

    protected override void CastAbilityFive()
    {
        if (!AbilityList[4].Usable())
        {
            return;
        }
        UnitAnimationSystem.TriggerBasicAttackAnimation();
        AnimationCooldown = AbilityList[4].SkillUsed();
        RpcPlayMuzzleFire();
        _judyProjectileComponent.UseLethalExplosion(TargetPoint, AbilityList[4].Id, AbilityList[4].Damage);

        Debug.Log("Ability Name " + AbilityList[4].name);
    }

    private void PlayMuzzleFire()
    {
        if (_muzzleFire.isPlaying)
        {
            _muzzleFire.Stop();
        }
        _muzzleFire.Play();
    }

    [ClientRpc]
    private void RpcPlayMuzzleFire()
    {
        PlayMuzzleFire();
    }

    [ClientRpc]
    private void RpcIniateLeapEffect()
    {
        StopCoroutine(ShadowLeapEffect());
        StartCoroutine(ShadowLeapEffect());
    }

    private IEnumerator ShadowLeapEffect()
    {
        MaterialPropertyBlock block = new MaterialPropertyBlock();
        block.SetFloat(_alphaClipValue,1);
        _renderer.SetPropertyBlock(block);
        yield return new WaitForSeconds(0.1f);
        float val = 1;
        float modifier = 0.1f;
        for (int i = 0; i < 10; i++)
        {
            val -= modifier;
            if(val<=0)
            {
                val = 0;
            }
            block.SetFloat(_alphaClipValue, val);
            _renderer.SetPropertyBlock(block);
            yield return new WaitForSeconds(0.1f);
        }
    }

    public override void AbilityTwo_performed(InputAction.CallbackContext obj)
    {
        CmdSelectedAbility(1);
    }


}
