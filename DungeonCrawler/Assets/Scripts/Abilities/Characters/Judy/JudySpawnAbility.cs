﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using Mirror;

[Serializable]
public class JudySpawnAbility : SpawnAbility
{
    [SerializeField]
    protected Transform[] _spawnPoints;

    protected override void Awake()
    {
        base.Awake();
    }

    public void UseTripleShotSkill(Vector3 endPos,int abilityID, int damage)
    {
        GameObject[] tripleShot = new GameObject[5];
        for (int i = 0; i < tripleShot.Length; i++)
        {
            GameObject go = _unitObjectPool.GetGameObjectFromPool(abilityID, _spawnPoint.position, _spawnPoints[i].rotation);
            go.SetActive(true);
            var projectile = go.GetComponent<ProjectileBase>();
            projectile.SetDamage(damage);
            projectile.StartProjectile(_spawnPoint.position, endPos);
            NetworkServer.Spawn(go);
            tripleShot[i] = go;
        }
   
        StartCoroutine(DestroyTripleShotAbilityObject(abilityID, tripleShot, 1.5f));
    }

    IEnumerator DestroyTripleShotAbilityObject(int abilityID,GameObject[] bullets, float delay)
    {
        yield return new WaitForSeconds(delay);

        for (int i = 0; i < bullets.Length; i++)
        {
            // return object to pool on server
            _unitObjectPool.AddToPool(abilityID, bullets[i]);

            // tell server to send ObjectDestroyMessage, which will call UnspawnHandler on client
            NetworkServer.UnSpawn(bullets[i]);
        }

    }

    public void UseShadowLotus(Vector3 endPos, int abilityID,int damage)
    {
        GameObject go = _unitObjectPool.GetGameObjectFromPool(abilityID, endPos, Quaternion.identity);
        go.SetActive(true);
        var trap = go.GetComponent<TrapBase>();
        trap.SetDamage(damage);
        NetworkServer.Spawn(go);

        StartCoroutine(DestroyShadowLotusAbilityObject(abilityID, go, 20f));
    }

    IEnumerator DestroyShadowLotusAbilityObject(int abilityID, GameObject trap, float delay)
    {
        yield return new WaitForSeconds(delay);

        // return object to pool on server
        _unitObjectPool.AddToPool(abilityID, trap);

        // tell server to send ObjectDestroyMessage, which will call UnspawnHandler on client
        NetworkServer.UnSpawn(trap);

    }

    public void UseShadowStep(Vector3 endPos, int abilityID)
    {
        GameObject go = _unitObjectPool.GetGameObjectFromPool(abilityID, endPos, Quaternion.identity);
        go.SetActive(true);
        NetworkServer.Spawn(go);
        StartCoroutine(DestroyShadowStepAbilityObject(abilityID, go, 1.5f));
    }

    IEnumerator DestroyShadowStepAbilityObject(int abilityID, GameObject go, float delay)
    {
        yield return new WaitForSeconds(delay);
        // return object to pool on server
        _unitObjectPool.AddToPool(abilityID, go);

        // tell server to send ObjectDestroyMessage, which will call UnspawnHandler on client
        NetworkServer.UnSpawn(go);
    }

    public void UseLethalExplosion(Vector3 endPos, int abilityID,int damage)
    {
        GameObject go = _unitObjectPool.GetGameObjectFromPool(abilityID, _spawnPoint.position, _spawnPoint.rotation);
        go.SetActive(true);
        var projectile = go.GetComponent<ProjectileBase>();
        projectile.SetDamage(damage);
        projectile.StartProjectile(_spawnPoint.position, endPos);
        NetworkServer.Spawn(go);
        StartCoroutine(DestroyLethalAbilityObject(abilityID, go, 1.5f));
    }

    IEnumerator DestroyLethalAbilityObject(int abilityID, GameObject go, float delay)
    {
        yield return new WaitForSeconds(delay);

        // return object to pool on server
        _unitObjectPool.AddToPool(abilityID, go);

        // tell server to send ObjectDestroyMessage, which will call UnspawnHandler on client
        NetworkServer.UnSpawn(go);
    }

}
