﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using Mirror;

public class SpawnAbility : NetworkBehaviour
{
    [SerializeField]
    protected Transform _spawnPoint;
    protected UnitPoolManager _unitObjectPool;

    protected virtual void Awake()
    {
        _unitObjectPool = FindObjectOfType<UnitPoolManager>();
    }


    public virtual void UseAbility(Vector3 endPos,int abilityID, int damage)
    {
        GameObject go = _unitObjectPool.GetGameObjectFromPool(abilityID, _spawnPoint.position, _spawnPoint.rotation);
        go.SetActive(true);
        var projectile = go.GetComponent<ProjectileBase>();
        projectile.SetDamage(damage);
        projectile.StartProjectile(_spawnPoint.position, endPos);
        NetworkServer.Spawn(go);
        StartCoroutine(DestroyBasicAbilityObject(abilityID, go, 1.5f));
    }

    IEnumerator DestroyBasicAbilityObject(int abilityID,GameObject go, float delay)
    {
        yield return new WaitForSeconds(delay);

        // return object to pool on server
        _unitObjectPool.AddToPool(abilityID, go);

        // tell server to send ObjectDestroyMessage, which will call UnspawnHandler on client
        NetworkServer.UnSpawn(go);
    }

}
