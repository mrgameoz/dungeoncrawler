﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class TrapBase : NetworkBehaviour
{ 
    [SerializeField]
    private float _explostionRadius;
    [SerializeField]
    private float _trapRadius;
    [SerializeField]
    private float selfDisableTime;
    [SerializeField]
    private GameObject trap;
    [SerializeField]
    private GameObject effect;

    private Collider mCollider;
    protected int Damage;

    private void Awake()
    {
        mCollider = GetComponent<Collider>();
    }

    private void OnEnable()
    {
        trap.SetActive(true);
        effect.SetActive(false);
        mCollider.enabled = true;
        if(mCollider is SphereCollider)
        {
            (mCollider as SphereCollider).radius = _trapRadius;
        }
        Invoke(nameof(CheckforEnemiesAround), 0.3f);

    }

    [ServerCallback]
    public void SetDamage(int damage)
    {
        Damage = damage;
    }


    [ServerCallback]
    private void CheckforEnemiesAround()
    {
       Collider[] affectedObjects = Physics.OverlapSphere(transform.position, _trapRadius);
        if(affectedObjects.Length>1)
        {
            for (int i = 0; i < affectedObjects.Length; i++)
            {
                GameObject affectedGameObject = affectedObjects[i].gameObject;
                if (affectedGameObject.gameObject.CompareTag("Enemy"))
                {
                    InitiateExplostion();
                    break;
                }
            }
        }

    }

    [ServerCallback]
    protected virtual void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            InitiateExplostion();
        }
    }

    private void InitiateExplostion()
    {
        Collider[] affectedObjects = Physics.OverlapSphere(transform.position, _explostionRadius);
        for (int i = 0; i < affectedObjects.Length; i++)
        {
            GameObject affectedGameObject = affectedObjects[i].gameObject;
            if (/*affectedGameObject.CompareTag("Enemy") &&*/
                affectedGameObject.TryGetComponent<UnitStatSystem>(out UnitStatSystem target))
                if (affectedGameObject.gameObject.CompareTag("Enemy"))
                {
                    target.ServerTakeDamage(Damage);
                }
        }
        RpcActivateTrap();//calls on client from server
    }

    [ClientRpc]
    protected void RpcActivateTrap()
    {
        ActivateTrap();
    }

    protected virtual void ActivateTrap()
    {
        mCollider.enabled = false;
        effect.SetActive(true);
        trap.SetActive(false);
        StartCoroutine(DisableEffectObject());
    }

    private IEnumerator DisableEffectObject()
    {
        yield return new WaitForSeconds(5);
        effect.SetActive(true);
    }
}
