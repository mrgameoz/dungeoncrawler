﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.AI;

public class DashBase : NetworkBehaviour
{
    [SerializeField]
    protected Transform raycastPoint;
    [SerializeField]
    protected LayerMask layerMask;
    [SerializeField]
    protected ParticleSystem _dashSmokeParticle;
    [SerializeField]
    protected ParticleSystem _dashParticle;
    [SerializeField]
    protected UnitPoolManager _unitObjectPool;
    protected Transform mTransform;

    //Aniamtion
    protected int _dashAmin = Animator.StringToHash("Dash");
    [SerializeField]
    protected Animator animator;
    [SerializeField]
    protected Renderer mRenderer;
    private string DashValue = "DashValue";

    protected virtual void Awake()
    {
        mTransform = transform;
        _unitObjectPool = FindObjectOfType<UnitPoolManager>();
    }

    public virtual void UseDashAbility(Vector3 endPos, float range,float duration,int abilityID)
    {
        Vector3 direction = (endPos - mTransform.position).normalized;
        RaycastHit hit;
        if(Physics.Raycast(raycastPoint.position,direction,out hit,range,layerMask))
        {
            if(hit.collider.gameObject.GameObjectIsObstacle())
            {
                Vector3 walldirection = (mTransform.position - hit.point).normalized;
                Vector3 finalPos = hit.point + walldirection;
                finalPos.y = 0;
                LeanTween.move(gameObject, finalPos, duration).setEase(LeanTweenType.easeOutQuad);
            }
        }
        else
        {
            LeanTween.move(gameObject, endPos, duration).setEase(LeanTweenType.easeOutQuad);
        }
        RpcEnableDashEffect(duration);
    }

    [ClientRpc]
    private void RpcEnableDashEffect(float duration)
    {
        _dashSmokeParticle.gameObject.SetActive(true);
        _dashParticle.gameObject.SetActive(true);
        animator.SetBool(_dashAmin, true);
        StopCoroutine(DashEffect(duration));
        StartCoroutine(DashEffect(duration));
    }

    IEnumerator DashEffect(float delay)
    {
        MaterialPropertyBlock block = new MaterialPropertyBlock();
        block.SetFloat(DashValue, 1);
        mRenderer.SetPropertyBlock(block);
        yield return new WaitForSeconds(delay);
        _dashSmokeParticle.Stop();
        _dashSmokeParticle.gameObject.SetActive(false);
        _dashParticle.Stop();
        _dashParticle.gameObject.SetActive(false);
        animator.SetBool(_dashAmin, false);
        float val = 1;
        float modifier = 0.1f;
        for (int i = 0; i < 10; i++)
        {
            val -= modifier;
            if (val <= 0)
            {
                val = 0;
            }
            block.SetFloat(DashValue, val);
            mRenderer.SetPropertyBlock(block);
            yield return new WaitForSeconds(0.1f);
        }
    }
}
