﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class ProjectileBase : NetworkBehaviour
{
    [SerializeField]
    protected float speed;
    [SerializeField]
    protected bool _passThroughEnemies;
    [SerializeField]
    protected GameObject bullet;
    [SerializeField]
    protected ParticleSystem bulletParticle;
    [SerializeField]
    protected GameObject onHitEffect;
    [SerializeField]
    protected ParticleSystem onHitEffectParticle;
    [SerializeField]
    protected float MaxDistance;
    protected Vector3 StartPos;
    protected Transform mTransform;
    protected Collider mCollider;
    private bool _move;
    protected int Damage;

    protected virtual void Awake()
    {
        mCollider = GetComponent<Collider>();
        mTransform = transform;
    }

    protected virtual void OnEnable()
    {
        _move = true;
        StartPos = mTransform.position;
        bullet.SetActive(true);
        onHitEffect.SetActive(false);
        mCollider.enabled = true;
    }

    [ServerCallback]
    public void SetMaxDistance(float distance)
    {
        MaxDistance = distance;
    }

    [ServerCallback]
    private void CheckDistanceTravlled()
    {
        if (!_move)
        {
            return;
        }
        Vector3 difference = mTransform.position - StartPos;
        if (Vector3.SqrMagnitude(difference) >= MaxDistance * MaxDistance)
        {
            DisableObject();
            RpcDisableObject();
        }
    }

    [ServerCallback]
    public void StartProjectile(Vector3 startPos,Vector3 endPos)
    {
        endPos.y = 1;
        transform.position = startPos;
    }

    [ServerCallback]
    public void SetDamage(int damage)
    {
        Damage = damage;
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        Movement();
        CheckDistanceTravlled();
    }

    [ServerCallback]
    protected virtual void Movement()
    {
        if(!_move)
        {
            return;
        }
        transform.position += transform.forward * speed * Time.deltaTime;
    }

    [ServerCallback]
    protected virtual void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.GameObjectIsObstacle())
        {
            DisableObject();
            RpcDisableObject();
        }

        if(other.gameObject.GameObjectIsEnemy())
        {
            if (other.gameObject.TryGetComponent<UnitStatSystem>(out UnitStatSystem target))
            {
                target.ServerTakeDamage(Damage);
                if(!_passThroughEnemies)
                {
                    DisableObject();
                    RpcDisableObject();
                }
            }
        }
    }

    [ClientRpc]
    protected void RpcDisableObject()
    {
        DisableObject();
    }

    protected virtual void DisableObject()
    {
        _move = false;
        bullet.SetActive(false);
        onHitEffect.SetActive(true);
        bulletParticle.Stop();
        mCollider.enabled = false;
        StartCoroutine(DisablOnHitEffect(2));
    }

    IEnumerator DisablOnHitEffect(float dur)
    {
        yield return new WaitForSeconds(dur);
        onHitEffect.SetActive(false);
        onHitEffectParticle.Stop();
        gameObject.SetActive(false);
    }


}
