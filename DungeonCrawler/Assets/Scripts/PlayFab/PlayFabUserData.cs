﻿using UnityEngine;
using PlayFab;
using PlayFab.ServerModels;
using System;

public static class PlayFabUserData
{
    public static string SessionTicket;
    public static string Username { get; private set; }

    public static void GetUsername(Action resultCallback = null)
    {
        PlayFabServerAPI.AuthenticateSessionTicket(new AuthenticateSessionTicketRequest
        {
            SessionTicket = SessionTicket
        },
        result =>
        {
            Username = result.UserInfo.CustomIdInfo.CustomId;
            resultCallback?.Invoke();
        }
        ,
        error =>
        {
            Debug.LogError(error.GenerateErrorReport());
        });
    }

}
