﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using PlayFab;
using PlayFab.ClientModels;
using System;

public class PlayFabLogin : MonoBehaviour
{

    [SerializeField]
    private TMP_Text _userID;

    public void Register()
    {
        var request = new LoginWithCustomIDRequest
        {
            CustomId = _userID.text,
            CreateAccount = true
        };

        PlayFabClientAPI.LoginWithCustomID(request, RegisterSuccess, RegisterFailure);
    }

    private void RegisterFailure(PlayFabError obj)
    {
        Debug.Log("Register Failed");
    }

    private void RegisterSuccess(LoginResult obj)
    {
        PlayFabUserData.SessionTicket = obj.SessionTicket;

        Debug.Log("Register Success");
    }

    public void Login()
    {
        var request = new LoginWithCustomIDRequest
        {
            CustomId = _userID.text,
            CreateAccount = false
        };

        PlayFabClientAPI.LoginWithCustomID(request, LoginSuccess, LoginFailure);
    }

    private void LoginFailure(PlayFabError obj)
    {
        Debug.Log("Login Failed");
    }

    private void LoginSuccess(LoginResult obj)
    {
        PlayFabUserData.SessionTicket = obj.SessionTicket;
        Debug.Log("Login Success");
    }
}
