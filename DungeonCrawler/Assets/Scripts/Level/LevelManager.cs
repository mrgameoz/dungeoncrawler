﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class LevelManager : NetworkBehaviour
{
    private LevelEnemySpawner _enemySpawner;

    private void Awake()
    {
        _enemySpawner = GetComponent<LevelEnemySpawner>();
    }

    public void Init()
    {
        _enemySpawner.CreateEnemies();
    }
}
