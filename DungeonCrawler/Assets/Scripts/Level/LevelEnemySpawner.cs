﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class LevelEnemySpawner : NetworkBehaviour
{
    [SerializeField]
    private GameObject[] _enemies;

    private List<GameObject> _enemyList = new List<GameObject>();
    [SerializeField]
    private Transform[] _enemySpawnPositions;
    [SerializeField]
    private int[] _enemyIndexToSpawn;

    [ServerCallback]
    public void CreateEnemies()
    {
        if(_enemyList.Count==0)
        {
            for (int i = 0; i < _enemyIndexToSpawn.Length; i++)
            {
                int enemyIndex = _enemyIndexToSpawn[i];
                GameObject go = Instantiate(_enemies[enemyIndex], _enemySpawnPositions[i].position, _enemySpawnPositions[i].rotation);
                go.GetComponent<EnemyUnitStatSystem>().InitEnemyStats();
                _enemyList.Add(go);
                NetworkServer.Spawn(go);
            }
        }
        else
        {
            SpawnEnemies();
        }
    }

    [ServerCallback]
    private void SpawnEnemies()
    {
        for (int i = 0; i < _enemyList.Count; i++)
        {
            NetworkServer.Spawn(_enemyList[i]);
        }
    }
}
