﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : UnitController
{
    protected AIStateController AIStateController;
    [SerializeField]
    protected ObserveTask ObserveTask;
    [SerializeField]
    protected ChaseTask ChaseTask;
    [SerializeField]
    protected AttackTask AttackTask;
    [SerializeField]
    protected FleeTask FleeTask;

    protected override void Awake()
    {
        base.Awake();
        AIStateController = GetComponent<AIStateController>();
    }

    private void OnEnable()
    {
        ObserveTask.Event_TargetFound += ChaseTask.SetTarget;
        ObserveTask.Event_TargetFound += UnitAbilitySystem.SetTarget;
    }

    private void OnDisable()
    {
        ObserveTask.Event_TargetFound -= ChaseTask.SetTarget;
        ObserveTask.Event_TargetFound -= UnitAbilitySystem.SetTarget;
    }

    protected override void Update()
    {
        if (UnitStatSystem.UnitState != UnitState.Dead)
        {
            UnitStatSystem.SafeUpdate();
            if (UnitAbilitySystem != null)
            {
                UnitAbilitySystem.SafeUpdate();
            }
            UnitMovementSystem.SafeUpdate();
            AIStateController.SafeUpdate();
        }
        UnitAnimationSystem.SafeUpdate(); 
    }
}
