﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
public class EnemyProjectileBase : ProjectileBase
{
    [ServerCallback]
    protected override void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GameObjectIsObstacle())
        {
            RpcDisableObject();
        }

        if (other.gameObject.GameObjectIsPlayer())
        {
            if (other.gameObject.TryGetComponent<UnitStatSystem>(out UnitStatSystem target))
            {
                target.ServerTakeDamage(Damage);
                if (!_passThroughEnemies)
                {
                    RpcDisableObject();
                }
            }
        }
    }
}
