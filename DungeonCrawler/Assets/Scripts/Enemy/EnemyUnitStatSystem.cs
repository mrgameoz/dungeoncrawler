﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyUnitStatSystem : UnitStatSystem
{
    protected override void InitStats()
    {
        base.InitStats();
        CurrentHealth = BaseStatsValue.Health;
        CurrentMana = BaseStatsValue.Mana;
    }

    public void InitEnemyStats()
    {
        InitStats();
    }

    public override void Revive()
    {
        InitStats();
    }
}
