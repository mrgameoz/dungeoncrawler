﻿using UnityEngine;
using System;
using Mirror;

public class EnemyAbilitySystem : UnitAbilitySystem
{
    [SerializeField]
    protected int[] AbilityChainIndex;
    protected int AbilityToUseIndex;

    protected SpawnAbility SpawnAbility;
    [SerializeField]
    protected ParticleSystem MuzzleFire;

    public float GlobalAbilityCooldown { private set; get; }
    public Action Event_GlobalAbilityCooldownCompleted;

    protected override void Awake()
    {
        base.Awake();
        SpawnAbility = GetComponent<SpawnAbility>();
    }

    [ServerCallback]
    public virtual void InitateAttack()
    {
        int abilityToCast = AbilityChainIndex[AbilityToUseIndex];
        UnitLookAtRotation.LookAtDirection(TargetGameObject.transform.position);
        AnimationCooldown = AbilityList[abilityToCast].SkillUsed();
        RpcPlayMuzzleFire();
        UnitAnimationSystem.TriggerBasicAttackAnimation();
        GlobalAbilityCooldown = AbilityList[abilityToCast].GlobalCooldown;
        Debug.Log("Attacked " + AbilityList[abilityToCast].Name);
        SpawnAbility.UseAbility(TargetPoint, AbilityList[abilityToCast].Id, AbilityList[abilityToCast].Damage);
        AbilityToUseIndex++;
        if(AbilityToUseIndex>=AbilityChainIndex.Length)
        {
            AbilityToUseIndex = 0;
        }
    }

    public override void SafeUpdate()
    {
        base.SafeUpdate();
        UpdateGlobalCooldown();
    }

    [ServerCallback]
    protected virtual void UpdateGlobalCooldown()
    {
        if (GlobalAbilityCooldown > 0)
        {
            GlobalAbilityCooldown -= Time.deltaTime;
            if (GlobalAbilityCooldown <= 0)
            {
                Event_GlobalAbilityCooldownCompleted?.Invoke();
                GlobalAbilityCooldown = 0;
            }
        }
    }

    [ServerCallback]
    public override void SetTarget(GameObject target)
    {
        base.SetTarget(target);
        TargetPoint = target.transform.position;
    }

    private void PlayMuzzleFire()
    {
        if (MuzzleFire.isPlaying)
        {
            MuzzleFire.Stop();
        }
        MuzzleFire.Play();
    }

    [ClientRpc]
    private void RpcPlayMuzzleFire()
    {
        PlayMuzzleFire();
    }
}
