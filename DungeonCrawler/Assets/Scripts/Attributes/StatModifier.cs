﻿using System;

//used for buff and debuff
[Serializable]
public class StatModifier
{
    public BaseStats Stats;
    public float _durationRemaining;
    public float _duration; //duration of modifier

    public void Update(float deltaTime)
    {
        _durationRemaining -= deltaTime;
        if (_durationRemaining <= 0)
        {
            _durationRemaining = 0;
        }
    }

    public void ApplyModifier()
    {
        _durationRemaining = _duration;
    }

    public float PercentageRemaining()
    {
        return _durationRemaining / _duration;
    }

    public bool IsFinished()
    {
        return _durationRemaining <= 0;
    }
}
