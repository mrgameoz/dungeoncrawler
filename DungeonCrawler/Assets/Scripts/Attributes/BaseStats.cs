﻿using System;
using UnityEngine;

[Serializable]
public class BaseStats 
{
    public enum BaseStatEnum { Health , Mana, Shield, Damage, MovementSpeed, AttackSpeed, CooldownReduction, CritChance, DamageReduction, HealthRegen };
    [Range(0,1000)]
    public int Health;
    public int Mana;
    public int Shield;
    public int Damage;
    public float MovementSpeed;
    public float AttackSpeed;
    public float CooldownReduction;
    public float CritChance;
    public float DamageReduction;
    public float HealthRegen;

    public BaseStats()
    {

    }

    //creates clone of reference
    public BaseStats(BaseStats referenceObject)
    {
        Clone(referenceObject);
    }

    //clone values of reference
    public void Clone(BaseStats referenceObject)
    {
        Health = referenceObject.Health;
        Mana = referenceObject.Mana;
        Shield = referenceObject.Shield;
        Damage = referenceObject.Damage;
        MovementSpeed = referenceObject.MovementSpeed;
        AttackSpeed = referenceObject.AttackSpeed;
        CooldownReduction = referenceObject.CooldownReduction;
        CritChance = referenceObject.CritChance;
        DamageReduction = referenceObject.DamageReduction;
        HealthRegen = referenceObject.HealthRegen;
    }


    public void AddValue(BaseStatEnum stat, int value)
    {
        switch (stat)
        {
            case BaseStatEnum.Health:
                {
                    Health += value;
                    break;
                }
            case BaseStatEnum.Mana:
                {
                    Mana += value;
                    break;
                }
            case BaseStatEnum.Shield:
                {
                    Shield += value;
                    break;
                }
            case BaseStatEnum.Damage:
                {
                    Damage += value;
                    break;
                }
            case BaseStatEnum.MovementSpeed:
                {
                    MovementSpeed += value;
                    break;
                }
            case BaseStatEnum.AttackSpeed:
                {
                    AttackSpeed += value;
                    break;
                }
            case BaseStatEnum.CooldownReduction:
                {
                    CooldownReduction += value;
                    break;
                }
            case BaseStatEnum.CritChance:
                {
                    CritChance += value;
                    break;
                }
            case BaseStatEnum.DamageReduction:
                {
                    DamageReduction += value;
                    break;
                }
            case BaseStatEnum.HealthRegen:
                {
                    HealthRegen += value;
                    break;
                }
        }
    }

    public void SubtractValue(BaseStatEnum stat, int value)
    {
        switch (stat)
        {
            case BaseStatEnum.Health:
                {
                    Health -= value;
                    break;
                }
            case BaseStatEnum.Mana:
                {
                    Mana -= value;
                    break;
                }
            case BaseStatEnum.Shield:
                {
                    Shield -= value;
                    break;
                }
            case BaseStatEnum.Damage:
                {
                    Damage -= value;
                    break;
                }
            case BaseStatEnum.MovementSpeed:
                {
                    MovementSpeed -= value;
                    break;
                }
            case BaseStatEnum.AttackSpeed:
                {
                    AttackSpeed -= value;
                    break;
                }
            case BaseStatEnum.CooldownReduction:
                {
                    CooldownReduction -= value;
                    break;
                }
            case BaseStatEnum.CritChance:
                {
                    CritChance -= value;
                    break;
                }
            case BaseStatEnum.DamageReduction:
                {
                    DamageReduction -= value;
                    break;
                }
            case BaseStatEnum.HealthRegen:
                {
                    HealthRegen -= value;
                    break;
                }
        }
    }

    public void SetValue(BaseStatEnum stat, int value)
    {
        switch (stat)
        {
            case BaseStatEnum.Health:
                {
                    Health = value;
                    break;
                }
            case BaseStatEnum.Mana:
                {
                    Mana = value;
                    break;
                }
            case BaseStatEnum.Shield:
                {
                    Shield = value;
                    break;
                }
            case BaseStatEnum.Damage:
                {
                    Damage = value;
                    break;
                }
            case BaseStatEnum.MovementSpeed:
                {
                    MovementSpeed = value;
                    break;
                }
            case BaseStatEnum.AttackSpeed:
                {
                    AttackSpeed = value;
                    break;
                }
            case BaseStatEnum.CooldownReduction:
                {
                    CooldownReduction = value;
                    break;
                }
            case BaseStatEnum.CritChance:
                {
                    CritChance = value;
                    break;
                }
            case BaseStatEnum.DamageReduction:
                {
                    DamageReduction = value;
                    break;
                }
            case BaseStatEnum.HealthRegen:
                {
                    HealthRegen = value;
                    break;
                }
        }
    }
}
