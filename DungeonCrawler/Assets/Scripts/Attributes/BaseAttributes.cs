﻿using System;
using UnityEngine;

[Serializable]
public struct BaseAttributes
{
    public int Stamina; //increases health
    public int Strength; //melee damage
    public int Agility; //attack speed
    public int Intelligence; //magic damage
    public int Crit; //crit chance
    public int Haste; //cooldown reduction
    public int Vitality; //improves defense
}
